package ExamemPOO;

import java.util.ArrayList;
import java.util.Random;

public class Magatzem {
	private static int numActual = 0;
	private static final int MaximRegals = 100;
	private static ArrayList<Regal> RegalsVenda = new ArrayList<Regal>();//STATIC??
	
	
	//CONSTRUCTOR
	public Magatzem() {
	}
	
	//MÉTODES
	public int nouRegal(Regal regalAlta) {
		if (numActual < MaximRegals) {
			RegalsVenda.add(regalAlta);
			numActual++;
			return numActual;
		} else
			return -1;
	}
	
	public Regal obtenirRegal(int posicio) {
		if (posicio>=0 && posicio<100) {
			return RegalsVenda.get(posicio);
		}else
			return null;
	}
	
	public void visualitzarMagatmem(String tipus) {
		if (tipus.toUpperCase().equals("ROSA")) {
			for (int i = 0; i < RegalsVenda.size(); i++) {
				if (RegalsVenda.get(i) instanceof Rosa) {
					System.out.println(RegalsVenda.get(i).toString());
				}
			}
		} else if (tipus.toUpperCase().equals("LLIBRE")) {
			for (int i = 0; i < RegalsVenda.size(); i++) {
				if (RegalsVenda.get(i) instanceof Llibre) {
					System.out.println(RegalsVenda.get(i).toString());
				}
			}
		}
	}
	
	public void omplirMagatzem(int num) {
		Random rd = new Random();
		for (int i = 0; i < num; i++) {
			//Generar de manera aleatoria un regal:
			int randTipus = rd.nextInt(2);//Determina si es una rosa o un llibre
			Regal regalPerAfegir = null;
			int randEstoc = rd.nextInt(149)+1;
			int randPreu = rd.nextInt(9)+1;
			String color = "";
			String qualitat = "";
			if (randTipus == 0) { // És una rosa
				int randColor = rd.nextInt(3); //Determinem el color de les roses
				if (randColor == 0) {
					color = "Blanc";
				} else if (randColor == 1) {
					color = "Vermell";
				} else {
					color = "Blau";
				}
				int randQualitat = rd.nextInt(3);
				if (randQualitat == 0) {
					qualitat = "Alta";
				} else if (randQualitat == 1) {
					qualitat = "Mitjana";
				} else {
					qualitat = "Baixa";
				}
				regalPerAfegir = new Rosa(randEstoc, randPreu, color, qualitat);
			} else { //És un llibre
				String titol = "Llibre num" + i;
				String[] generes = {"Comedia", "Drama", "Ciencia Ficció", "Poesia", "Infantil", "Cientific"}; 
				EnquaLlibre enq = null;
				int randEnq = rd.nextInt(2);
				if (randEnq==0) {
					enq = EnquaLlibre.RUSTICA;
				} else if (randEnq == 1) {
					enq = EnquaLlibre.TAPADURA;
				}
				int randGenere = rd.nextInt(6);
				String genere = generes[randGenere];
				regalPerAfegir = new Llibre(randEstoc, randPreu, titol, enq, genere);
			}
			if (regalPerAfegir != null) {
				this.nouRegal(regalPerAfegir);
			}
			
		}
		
	}
	
	
	public float vendaRegal(int pos) {
		float num = 0;
		if (pos>numActual && pos>=0)
			return num;
		else {
			if(RegalsVenda.get(pos).stock>0) {
				RegalsVenda.get(pos).setStock(RegalsVenda.get(pos).getStock()-1);
				num = RegalsVenda.get(pos).getPreuVenda();
			}
			if (RegalsVenda.get(pos).stock==0) {
				RegalsVenda.remove(pos);
			}
		}
		return num;
	}
	
	public void regalsEquivalents(Regal reg) {
		for (int i = 0; i < RegalsVenda.size(); i++) {
			if (reg.equals(RegalsVenda.get(i))) {
				System.out.println(RegalsVenda.get(i).toString());
			}
		}
	}
	
	public float sumaRegals(Regal reg) {
		float sumaTotal = 0;
		for (int i = 0; i < RegalsVenda.size(); i++) {
			if (reg.equals(RegalsVenda.get(i))) {
				sumaTotal += RegalsVenda.get(i).getPreuVenda();
			}
		}
		return sumaTotal;
	}
	
	public float mitjanaRegals(Regal reg) {
		float sumaTotal = 0;
		int cont = 0;
		for (int i = 0; i < RegalsVenda.size(); i++) {
			if (reg.equals(RegalsVenda.get(i))) {
				sumaTotal += RegalsVenda.get(i).getPreuVenda();
				cont++;
			}
		}
		return sumaTotal/cont;
	}
	
	
	public int getNumActual() {
		return numActual;
	}

}
