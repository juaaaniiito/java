package ExamemPOO;

public abstract class Regal {
	protected float preuVenda = 10;
	protected int stock = 100;
	protected final static float PREU_BASE_MAXIM = 10;
	
	//GETTERS I SETTERS
	
	public float getPreuVenda() {
		return preuVenda;
	}
	public int getStock() {
		return stock;
	}
	public void setPreuVenda(float preuVenda) {
		if (preuVenda >= 0)
			this.preuVenda = preuVenda;
	}
	public void setStock(int stock) {
		if (stock >= 0)
			this.stock = stock;
	}
	
	//CONSTRUCTORS DE L'OBJECTE
	public Regal() {
		
	}
	
	public Regal(float preuVenda, int stock) {
		this();
		if (comprovarPreuBase(preuVenda)) {
			this.preuVenda = preuVenda;
			this.stock = stock;
		}
	}
	
	// MÈTODES DE LA CLASSE 'Regal'
	
	protected boolean comprovarPreuBase(float preu) {
		boolean correcte = false;
		if (preu > 0 && preu <= PREU_BASE_MAXIM)
			correcte = true;
		return correcte;
	}
	
	//TODO No sé que ha de retornar
	public abstract void preuFinal();
	
	
	@Override
	public String toString() {
		return "DADES DEL REGAL:\nPreuVenda: " + preuVenda + "\nStock: " + stock;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj == null)
			return false;
		if (obj.getClass() == this.getClass()) { //Del mateix tipus
			if (((Regal)obj).preuVenda == this.preuVenda) //Del mateix preu
				return true; 
		} else { //De tipus diferents
			if (this instanceof Llibre) {//Si això és un Llibre significa que 'obj' és una Rosa
				if (((Llibre)this).getPreuVenda() <= ((Rosa)obj).getPreuVenda()*2)
					return true;
			} else if (this instanceof Rosa) { //No faig servir un else per si de cas s'afegeix una nova classe que depengui de Regal.
				if (((Llibre)obj).getPreuVenda() <= ((Rosa)this).getPreuVenda()*2)
					return true;
			}
		}
		return false; //RETURN FALSE
	}
	
}
