package ExamemPOO;

public class Llibre extends Regal implements GenereLiterari{
	private String titol = "Sense títol";
	private EnquaLlibre enquadernacio = EnquaLlibre.RUSTICA;
	private String genere;
	
	//GETTERS
	public String getTitol() {
		return titol;
	}

	public String getGenere() {
		return genere;
	}

	
	//CONSTRUCTORS
	public Llibre() {
		super();
	}
	
	
	public Llibre(float preu, String titol) {
		super();
		if (this.comprovarPreuBase(preu))
			this.setPreuVenda(preu);
		this.titol = titol;
		preuFinal();
	}
	
	public Llibre(int estoc, float preu, String titol, EnquaLlibre enquadernacio, String genere) {
		super(preu, estoc);
		this.titol = titol;
		this.enquadernacio = enquadernacio;
		this.genere = genere;
		preuFinal();
	}
	
	//MÉTODES DE L'OBJECTE 'Llibre'
	
	@Override
	public void preuFinal() {
		if (this.enquadernacio == EnquaLlibre.TAPADURA)
			this.setPreuVenda(this.getPreuVenda()+5);
	}

	@Override
	public String toString() {
		String res = "";
		res = super.toString();
		res += "\nDades del llibre:\n    Titol: " + titol + "\n    Enquadernacio: " + enquadernacio;
		if (genere != null) {
			res += "\n    Genere: " + genere;
		}
		return res;
	}

	@Override
	public void actualitzarPreu() {
		float Percentatge = aplicarPercentatge(this.genere);
		this.setPreuVenda(this.getPreuVenda() *Percentatge);
	}

}
