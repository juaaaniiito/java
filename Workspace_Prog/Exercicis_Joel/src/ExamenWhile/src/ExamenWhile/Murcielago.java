package ExamenWhile.src.ExamenWhile;

import java.util.Scanner;

public class Murcielago {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String frase;
		frase = src.nextLine(); //Introduim la frase
		int a, e, i, o, u, llargada;
		a=e=i=o=u=0;
		frase=frase.toUpperCase();//Passem la frase a majúscules per no tenir problemes
		llargada = frase.length();
		char lletra;
		while(llargada>0) { // S'ha de recòrrer tots els càracters de la frase
			lletra= frase.charAt(llargada-1);
			switch(lletra) { //Busquem una vocal i la sumem als contadors
			case 'A':
				a++;
				break;
			case 'E':
				e++;
				break;
			case 'I':
				i++;
				break;
			case 'O':
				o++;
				break;
			case 'U':
				u++;
				break;
			}
			llargada--; // Canviem de càracter
		}
		if (a>0&&e>0&&i>0&&o>0&&u>0) { //Si conté totes les vocals:
			System.out.println("TOTES");
		} else {
			System.out.println("FALTEN: " ); //Especifiquem les vocals que falten
			if (a==0) {
				System.out.print("a");
			}
			if (e==0) {
				System.out.print("e");
			}
			if (i==0) {
				System.out.print("i");
			}
			if (o==0) {
				System.out.print("o");
			}
			if (u==0) {
				System.out.print("u");
			}
		}
			
	}

}
