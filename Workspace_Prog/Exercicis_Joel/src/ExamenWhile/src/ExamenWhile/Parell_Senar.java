package ExamenWhile.src.ExamenWhile;

import java.util.Scanner;

public class Parell_Senar {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int num;
		int senars=0;
		int parells=0;
		while(senars-parells!=3 && parells-senars!=3) { //En cas de que alguna de les dues restes arribi a 3, deixem de rebre nombres.
			num= src.nextInt();
			if (num%2==0) { //Depenent si és parell o senar ho sumem als contadors.
				parells++;
			} else {
				senars++;
			}
		}
		System.out.println("Has introduït "+parells + " parells i "+senars+ " senars.");

	}

}
