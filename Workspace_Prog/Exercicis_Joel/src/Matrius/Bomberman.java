package Matrius;

import java.util.Scanner;

public class Bomberman {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int files, columnes;
		files = src.nextInt();
		columnes = src.nextInt();
		int[][] matrix = new int[files][columnes];
		for (int i = 0; i < files; i++)
			for (int j = 0; j < columnes; j++)
				matrix[i][j] = src.nextInt();
		int posX, posY, acum;
		posX = src.nextInt();
		posY = src.nextInt();
		acum = 0;
		for (int i = 0; i < columnes; i++) {
			acum += matrix[posX][i];
		}
		for (int i = 0; i < files; i++) {
			acum += matrix[i][posY];
		}
		acum = acum - matrix[posX][posY];
		System.out.println(acum);

	}

}
