package Matrius;

import java.util.Scanner;

public class ClientesExcluyentes {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String[][] cine = new String[7][7];
		for (int i = 0; i < 7; i++)
			for (int j = 0; j < 7; j++)
				cine[i][j] = "X";
		for (int i = 1; i < 6; i++)
			for (int j = 1; j < 6; j++)
				cine[i][j] = src.next();
		Boolean ilegal = false;
		for (int i = 1; i < 6; i++)
			for (int j = 1; j < 6; j++)
				for (int m = i - 1; m < i + 2; m++)
					for (int k = j - 1; k < j + 2; k++)
						switch (cine[i][j]) {
						case ("A"):
							if (cine[m][k].equals("B"))
								ilegal = true;
							break;
						case ("C"):
							if (cine[m][k].equals("D"))
								ilegal=true;
						}
		if (!ilegal) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
	}
}