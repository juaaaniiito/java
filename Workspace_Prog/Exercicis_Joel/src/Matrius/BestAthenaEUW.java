package Matrius;

import java.util.Scanner;

public class BestAthenaEUW {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos= src.nextInt();
		for (; casos>0;casos--) {
			int f= src.nextInt();
			int c= src.nextInt();
			int[][] terreny= new int[f+2][c+2];
			
			for (int i= 0; i<f+2; i++)
				for (int j=0; j<c+2;j++) {
					if (i==0 || i== f+1 || j == 0|| j== c+1) {
						terreny[i][j]= 0;
					} else
						terreny[i][j]= src.nextInt();
				}
			int posx= src.nextInt()+1;
			int posy= src.nextInt()+1;
			int cont_enem=0;
			if (terreny[posy][posx]!=1) {
				System.out.println(-1);
			} else {
				if (terreny[posy-1][posx-1]==2)
					cont_enem++;
				if (terreny[posy-1][posx]==2)
					cont_enem++;
				if (terreny[posy-1][posx+1]==2)
					cont_enem++;
				if (terreny[posy][posx-1]==2)
					cont_enem++;
				if (terreny[posy][posx+1]==2)
					cont_enem++;
				if (terreny[posy+1][posx-1]==2)
					cont_enem++;
				if (terreny[posy+1][posx]==2)
					cont_enem++;
				if (terreny[posy+1][posx+1]==2)
					cont_enem++;
				System.out.println(cont_enem);
			}
		}
	}
}