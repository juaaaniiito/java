package Matrius;

import java.util.Scanner;

public class MaximDeMatriu {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos=src.nextInt();
		for (;casos>0;casos--) {
			int f= src.nextInt();
			int c= src.nextInt();
			int[][] matrix = new int[f][c];
			for (int i =0; i<f;i++)
				for(int j=0;j<c;j++)
					matrix[i][j]= src.nextInt();
			int maxim= Integer.MIN_VALUE;
			int posicio_f=0;
			int posicio_c=0;
			for (int i=0; i<f;i++)
				for(int j=0; j<c;j++)
					if (matrix[i][j]>maxim) {
						maxim=matrix[i][j];
						posicio_f=i+1;
						posicio_c=j+1;
					}
			System.out.println(posicio_f +" "+ posicio_c);
		}
	}
}