package Matrius;

import java.util.Scanner;

public class Notes1 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos= src.nextInt();
		for (;casos>0;casos--) {
			int num_alumnes=src.nextInt();
			int num_notes=src.nextInt();
			int[][] quali= new int[num_alumnes][num_notes];
			for (int i=0; i<num_alumnes;i++)
				for (int j=0;j<num_notes;j++)
					quali[i][j]= src.nextInt();
			for(int i=0;i<num_alumnes;i++) {
				int suma=0;
				for(int j=0; j<num_notes;j++)
					suma+=quali[i][j];
				System.out.print(suma/num_notes + " ");
			}
		}
	}
}