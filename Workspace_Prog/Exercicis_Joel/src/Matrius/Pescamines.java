package Matrius;

import java.util.Scanner;

public class Pescamines {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos= src.nextInt();
		for (; casos>0;casos--) {
			int inutil = src.nextInt();
			int f = src.nextInt();
			int c = src.nextInt();
			int[][] pescar = new int[f+2][c+2];
			for (int i=0; i<f+2; i++)
				for (int j=0;j<c+2; j++) {
					if (i==0 || i==f+1 || j==0 || j==c+1)
						pescar[i][j]=0;
				}
			for (int i=1; i<f+1; i++)
				for (int j=1;j<c+1; j++) {
					pescar[i][j]= src.nextInt();
				}
			int x= src.nextInt()+1;
			int y= src.nextInt()+1;
			int cont_mines=0;
			for (int i = y-1;i<y+2;i++)
				for (int j = x-1; j<x+2;j++) {
					if (pescar[i][j]==1) {
						cont_mines++;
					}
				}
			if (pescar[y][x]==1) {
				System.out.println("BOOM");
			} else {
				System.out.println(cont_mines);
			}
		}
	}
}