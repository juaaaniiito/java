package Matrius;

import java.util.Scanner;

public class PrimalAspid {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		for (; casos > 0; casos--) {
			int filaI = src.nextInt();
			int colI = src.nextInt();
			int filaO = src.nextInt();
			int colO = src.nextInt();
			Boolean fallat = false;
			if (filaI < filaO)
				fallat = true;
			if (filaI == filaO && colI < colO)
				fallat = true;
			if (colI == colO && filaI < filaO)
				fallat = true;
			if (fallat)
				System.out.println("MISS");
			if (!fallat) {
				int[][] hollow= new int[filaI+1][colI+1];
				for (int i=filaO; i<filaI+1; i++) {
					for (int j = colO; j<colI+1; j++) {
						if (i==j)
							hollow[i][j]=1;
					}
				}
				Boolean caught=false; 
				if (filaI == filaO && colI > colO) {
					System.out.println("HIT");
					caught=true;
				}	
				if (colI == colO && filaI > filaO) {
					System.out.println("HIT");
					caught=true;
				}
				if (hollow[filaI][colI]==1) {
					System.out.println("HIT");
					caught=true;
				}
				if (!caught)
					System.out.println("MISS");			
			}
		}
	}
}