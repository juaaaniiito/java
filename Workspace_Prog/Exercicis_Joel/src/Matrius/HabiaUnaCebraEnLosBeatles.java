package Matrius;

import java.util.Scanner;

public class HabiaUnaCebraEnLosBeatles {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int f = src.nextInt();
		int c = src.nextInt();
		int[][] cebra = new int[f][c];
		int comenca = src.nextInt();
		for (int i=0; i<f;i++) {
			for (int j=0; j<c;j++) {
				if (i==comenca) {
					for (int m=0;m<c;m++)
						cebra[i][m]=1;
					comenca+=2;
				}
				System.out.print(cebra[i][j]+" ");
			}
			System.out.println();
		}
	}
}