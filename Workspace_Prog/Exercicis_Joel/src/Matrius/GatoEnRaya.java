package Matrius;

import java.util.Scanner;

public class GatoEnRaya {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		for (; casos > 0; casos--) { //En el segundo caso, hay que mirar diagonales.
			int f = src.nextInt();
			int c = src.nextInt();
			int[][] gats = new int[f + 2][c + 2];
			for (int i = 1; i < f + 1; i++)
				for (int j = 1; j < c + 1; j++)
					gats[i][j] = src.nextInt();
			for (int i = 0; i < f + 2; i++)
				for (int j = 0; j < c + 2; j++)
					if (gats[i][j] != 1)
						gats[i][j] = 0;
			int cont_gats=0;
			for (int i=1; i < f +1; i++)
				for (int j= 1; j < c + 1; j++) {
					if(gats[i][j]==1) {
						if(gats[i][j+1]==1) {
							if(gats[i][j+2]==1) {
								cont_gats++;
								gats[i][j]=0;
								gats[i][j+1]=0;
								gats[i][j+2]=0;
							}
						} else if(gats[i][j-1]==1) {
							if(gats[i][j-2]==1){
								cont_gats++;
								gats[i][j]=0;
								gats[i][j-1]=0;
								gats[i][j-2]=0;
							}
						} else if (gats[i+1][j]==1) {
							if (gats[i+2][j]==1) {
								cont_gats++;
								gats[i][j]=0;
								gats[i+1][j]=0;
								gats[i+2][j]=0;
							}
						} else if (gats[i-1][j]==1) {
							if (gats[i-2][j]==1) {
								cont_gats++;
								gats[i][j]=0;
								gats[i-1][j]=0;
								gats[i-2][j]=0;
							}
						}	
					}
				}
			System.out.println(cont_gats);
			for (int i = 0; i < f + 2; i++) {
				for (int j = 0; j < c + 2; j++)
					System.out.print(gats[i][j] + " ");
				System.out.println();
			}
		}
	}
}