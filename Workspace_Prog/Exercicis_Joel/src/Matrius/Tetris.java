package Matrius;

import java.util.Scanner;

public class Tetris {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int f = src.nextInt();
		int c = src.nextInt();
		int[][] tetris = new int[f][c];
		int cont = 0;
		for (int i = 0; i < f; i++)
			for (int j = 0; j < c; j++)
				tetris[i][j] = src.nextInt();
		for (int i = 0; i < f; i++) {
			Boolean cero = false;
			for (int j = 0; j < c; j++) {
				if (tetris[i][j] == 0)
					cero = true;
			}
			if (!cero)
				cont++;
		}
		if (cont == 4) {
			System.out.println("TETRIS");
		} else {
			System.out.println(cont);
		}
	}

}
