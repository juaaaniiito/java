package Matrius;

import java.util.Scanner;

public class SumaFilesiColumnes {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int F=src.nextInt();
		int C=src.nextInt();
		int[][] matrix=new int[F][C];
		
		for(int i=0; i<F;i++)
			for(int j=0;j<C;j++)
				matrix[i][j]= src.nextInt();
		int sum_fil=0;
		int sum_col=0;
		int num=src.nextInt();
		for(int i=0;i<C;i++) {
			sum_fil+=matrix[num][i];
		}
		for(int i=0;i<F;i++) {
			sum_col+=matrix[i][num];
		}
		System.out.println(sum_fil+" "+sum_col);
	}
}