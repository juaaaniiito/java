package Matrius;

import java.util.Scanner;

public class IndexOfMatriu {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int F=src.nextInt();
		int C=src.nextInt();
		int[][] matrix = new int[F][C];
		
		for(int i=0; i<F;i++) 
			for(int j=0;j<C;j++) 
				matrix[i][j]= src.nextInt();
		
		int k = src.nextInt();
		Boolean trobat= false;
		for(int i=0; i<F;i++)
			for(int j=0;j<C;j++) 
				if (matrix[i][j]==k) {
					trobat=true;
					System.out.println(i +" " +j);
				}
		if (!trobat)
			System.out.println("-1 -1");
	}
}