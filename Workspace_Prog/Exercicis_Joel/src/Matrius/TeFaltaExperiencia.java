package Matrius;

import java.util.Scanner;

public class TeFaltaExperiencia {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int nivell_actual= src.nextInt();
		int exp_actual= src.nextInt();
		int limit= src.nextInt();
		String rangs=src.next("Escoge el rango de la mazmorra: Valores posibles: A, B o C");
		int valor_rang;
		switch (rangs) {
		case("S"): valor_rang=70; break;
		case("A"): valor_rang=50; break;
		case("B"): valor_rang=30; break; 
		case("C"): valor_rang=20; break;
		case("D"): valor_rang=10; break;
		case("E"): valor_rang=5; break;
		default: valor_rang=0;
		}
		int[][] mazmorra = new int[5][5];
		for(int i=0;i<5;i++)
			for(int j=0;j<5;j++)
				mazmorra[i][j]= src.nextInt();
		int cont_nivells=0;
		for(int i=0;i<5;i++) {
			for(int j=0; j<5;j++) {
				int exp_sum=mazmorra[i][j]*valor_rang;
				exp_actual+=exp_sum;
				if(exp_actual>=limit) {
					cont_nivells++;
					exp_actual= (int) (exp_actual-limit);
					limit=(int) ((int) limit*1.2);
				}
			}
		}
		int nou_nivell=nivell_actual+cont_nivells;
		System.out.println("HAS SUBIDO "+ cont_nivells+" NIVELES");
		System.out.println("EXP ACTUAL: "+ exp_actual);
		System.out.println("EXP SIGUIENTE NIVEL: "+ limit);
		System.out.println("NIVEL: "+nou_nivell);
		if (cont_nivells>=5) {
			System.out.println("ESTAS LISTO PARA LUCHAR CONTRA EL BOSS");
		} else {
			System.out.println("NO ESTAS PREPARADO PARA LUCHAR CONTRA EL BOSS");
		}
	}

}
