package Matrius;

import java.util.Random;
import java.util.Scanner;

public class PrimeraMatriu {

	public static void main(String[] args) {
		Random rd = new Random(); // TODO Crear una variable "RANDOM"
		Scanner src = new Scanner(System.in);
		final int TOPE = 5; // TODO crear una constant ('variable' que no varia)
		final int FILES = 8;
		final int COLUMNES = 8;
		int vector[] = new int[TOPE];
		int matriu[][] = new int[FILES][COLUMNES];
		/*
		 * for (int i = 0; i < TOPE; i++) { // TODO Rellenar un vector con random
		 * vector[i] = rd.nextInt(5, 10); } for (int i = 0; i < TOPE; i++) { // Mostrar
		 * un vector amb espais. System.out.print(vector[i] + " "); }
		 */
		/*
		 * for (int i = 0; i < FILES; i++) { // TODO Posar valors a matriu 
		 * for (int j = 0; j < COLUMNES; j++) matriu[i][j] = rd.nextInt(2); }
		 */
		
		int omplir=1;
		for (int i = 0; i < FILES; i++)
			for (int j = 0; j < COLUMNES; j++)
				matriu[i][j]= omplir++;
		
		
		for (int i = 0; i < FILES; i++) { // TODO Mostrar matriu per pantalla
			for (int j = 0; j < COLUMNES; j++)
				System.out.print(matriu[i][j] + " ");
			System.out.println();
		}
	}

}
