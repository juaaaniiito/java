package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class ooBloodborneVersioArrays {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos= src.nextInt();
		for(; casos>0; casos--) {
			int K = src.nextInt();
			ArrayList<Integer> larray = new ArrayList<Integer>();
			for(;K>0;K--) {
				larray.add(src.nextInt());
			}
			Boolean trobat = false;
			int pos=1;
			while(pos<larray.size()&& !trobat) {
				if(larray.get(pos)==larray.get(pos-1)) {
					trobat=true;
				}
				pos++;
			}
			if (!trobat) {
				System.out.println("NO");
			}else {
				System.out.println("SI");
			}
		}
	}
}