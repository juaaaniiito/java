package Arrays;

import java.util.Scanner;

public class Repetits {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos=src.nextInt();
		for(int i=0;i<casos;i++) {
			int K= src.nextInt();
			int[] larray = new int[K];
			for (int i2=0;i2<K;i2++) {
				larray[i2]= src.nextInt();
			}
			Boolean repetit= false;
			for (int i2=0;i2<K;i2++) {
				for(int i3=0;i3<K;i3++) {
					if (i3!=i2) {
						if (larray[i2]==larray[i3]) {
							repetit=true;
						}
					}
				}
			}
			if (repetit==true) {
				System.out.println("SI");
			}else {
				System.out.println("NO");
			}
		}
	}
}
