package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class OsShipeo {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos= src.nextInt();
		for(; casos>0;casos--) {
			int N= src.nextInt();
			ArrayList<Integer> larray = new ArrayList<Integer>();
			for(int i=0;i<N;i++) {
				larray.add(src.nextInt());
			}
			int minim, maxim;
			minim=Integer.MAX_VALUE;
			maxim=Integer.MIN_VALUE;
			for (int i =0;i<N;i++) {
				if (larray.get(i)<minim) {
					minim=larray.get(i);
				}
				if (larray.get(i)>maxim) {
					maxim=larray.get(i);
				}
			}
			System.out.println(maxim-minim);			
		}
	}
}