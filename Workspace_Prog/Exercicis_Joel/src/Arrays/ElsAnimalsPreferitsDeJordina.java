package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class ElsAnimalsPreferitsDeJordina {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		for(;casos>0;casos--) {
			int K = src.nextInt();
			src.nextLine();
			ArrayList<String> larray = new ArrayList<String>();
			for(;K>1;K--) {
				larray.add(src.nextLine());
			}
			String buscar = src.nextLine();
			if (larray.contains(buscar)) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
			
		}
	}
}