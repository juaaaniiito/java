package Arrays;

import java.util.Scanner;

public class TenRecordesDe {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		for (int casos=src.nextInt();casos>0;casos--) {
			final int K=src.nextInt();
			int[] larray = new int[K];
			for(int pos=0;pos<K;pos++) {
				larray[pos]= src.nextInt();
			}
			int P =src.nextInt();
			System.out.println(larray[P]);
		}
		
	}

}
