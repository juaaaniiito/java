package Arrays;

import java.util.Arrays;
import java.util.Scanner;

public class OrdenaAlfabeticamente {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos=src.nextInt();
		src.nextLine();
		for(;casos>0;casos--) {
			String frase=src.nextLine();
			frase=frase.replaceAll(" ", "");
			char[] caracters = frase.toCharArray();
			Arrays.sort(caracters);
			int pos=0;
			while (pos<caracters.length) {
				System.out.print(caracters[pos]);
				pos++;
			}
		}
	}
}