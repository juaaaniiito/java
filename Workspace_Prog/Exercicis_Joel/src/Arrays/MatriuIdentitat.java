package Arrays;

import java.util.Scanner;

public class MatriuIdentitat {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int fic = src.nextInt();
		int[][] mani = new int[fic][fic];
		for (int i = 0; i < fic; i++) {
			for (int j = 0; j < fic; j++) {
				if (i==j)
					mani[i][j]= 1;
				if(i!=j)
					mani[i][j]=0;
				System.out.print(mani[i][j]+" ");
			}
			System.out.println();
		}
	}

}
