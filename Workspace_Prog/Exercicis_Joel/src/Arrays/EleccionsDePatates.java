package Arrays;
import java.util.ArrayList;
import java.util.Scanner;

public class EleccionsDePatates {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos= src.nextInt();
		for(; casos>0;casos--) {
			int K=src.nextInt();
			ArrayList<Integer> larray= new ArrayList<Integer>();
			for (int i =0; i<K;i++) {
				larray.add(src.nextInt());
			}
			int ajuda=Integer.MIN_VALUE;
			int pos_guany=0;
			for(int i =0; i<K;i++) {
				if (larray.get(i)>ajuda) {
					ajuda=larray.get(i);
					pos_guany=larray.indexOf(ajuda)+1;
				}
			}
			System.out.println(pos_guany);
		}
	}
}