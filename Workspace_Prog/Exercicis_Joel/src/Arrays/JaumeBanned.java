package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class JaumeBanned {

	public static void main(String[] args) {
		Scanner src=new Scanner(System.in);
		ArrayList<String> par_banned= new ArrayList<String>();
		par_banned.add("HADA");
		par_banned.add("UWU");
		par_banned.add("OWO");
		par_banned.add("SACAPUNTAS");
		par_banned.add("ADOLFITO");
		par_banned.add("35");
		int casos= src.nextInt();
		src.nextLine();
		for (;casos>0;casos--) {
			String frase= src.nextLine();
			frase=frase.toUpperCase();
			String[] comentari = frase.split(" ");
			int pos=0;
			Boolean segur=true;
			while(pos<comentari.length&&segur==true) {
				if(par_banned.contains(comentari[pos])) {
					segur=false;
				}
				pos++;
			}
			if (segur==false) {
				System.out.println("Jaime ha recibido un Ban");
			} else {
				System.out.println("No Ban a Jaime");
			}
		}
	}
}