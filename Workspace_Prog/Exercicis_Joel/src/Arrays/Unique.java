package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class Unique {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos=src.nextInt();
		for (;casos>0;casos--) {
			ArrayList<String> larray = new ArrayList<String>();
			int K= src.nextInt();
			src.nextLine();
			larray.add(src.nextLine());
			for (int i =1;i<K;i++) {
				String paraula_nova=src.nextLine();
				if (!larray.contains(paraula_nova)) {
					larray.add(paraula_nova);
				}
			}
			System.out.println(larray);
		}
	}
}