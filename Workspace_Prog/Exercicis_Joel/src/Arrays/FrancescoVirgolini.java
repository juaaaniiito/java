package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class FrancescoVirgolini {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		for(int casos=src.nextInt();casos>0;casos--) {
			ArrayList<String> cursa= new ArrayList<String>();
			int num_cotxes=src.nextInt();
			src.nextLine();
			for (int i=0;i<num_cotxes;i++) {
				cursa.add(src.nextLine());
			}
			Boolean fran=false;
			int pos_comp=0;
			while(pos_comp<num_cotxes&&fran==false) {
				if (cursa.get(pos_comp).equalsIgnoreCase("Francesco Virgolini")) {
					fran=true;
				} else {
					pos_comp++;
				}
			}
			String guardem= cursa.get(pos_comp-1);
			cursa.set(pos_comp-1, "Francesco Virgolini");
			cursa.set(pos_comp, guardem);
			System.out.println(cursa);
			
		}

	}

}
