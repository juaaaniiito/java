package Arrays;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class FormulaDeuler {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos= src.nextInt();
		for(; casos>0;casos--) {
			int nums= src.nextInt();
			ArrayList<Integer> larray = new ArrayList<Integer>();
			for (int i =0; i<nums;i++) {
				larray.add(src.nextInt());
			}
			Collections.sort(larray);// TODO Ordenar un ArrayList
			int max=larray.size()-1;
			for (int i =0; i<nums/2;i++) {
				System.out.println(larray.get(i)+larray.get(max)+ " ");
				max--;
			}
		}
	}
}