package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class NotaDactitud2 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos= src.nextInt();
		for(; casos>0; casos--) {
			int N = src.nextInt();
			ArrayList<Integer> larray = new ArrayList<Integer>();
			for (int i=0; i<N;i++) 
				larray.add(src.nextInt());
			ArrayList<Integer> larray2 = new ArrayList<Integer>();
			for (int i=0; i<N;i++) 
				larray2.add(src.nextInt());
			int cont=0;
			int max= larray2.get(0)-larray.get(0);
			for (int i=0; i<N;i++) {
				if(larray2.get(i)-larray.get(i)==max) {
					cont++;
				}
				if (larray2.get(i)-larray.get(i)>max) {
					max=larray2.get(i)-larray.get(i);
					cont=1;
				}
			}
			System.out.println(max+ " " +cont);
		
		}
	}

}
