package Arrays;

import java.util.Scanner;

public class ABBA {

	public static void main(String[] args) {
		Scanner src = new Scanner (System.in);
		int casos= src.nextInt();
		int pos;
		src.nextLine();
		while(casos>0) {
			String frase=src.nextLine();
			frase=frase.toUpperCase();
			pos=0;
			frase=frase.replace('À', 'A');
			frase=frase.replace('Á', 'A');
			frase=frase.replace('È', 'E');
			frase=frase.replace('É', 'E');
			frase=frase.replace('Í', 'I');
			frase=frase.replace('Ò', 'O');
			frase=frase.replace('Ó', 'O');
			frase=frase.replace('Ú', 'U');
			String[] noms= frase.split(", ");
			pos=0;
			while(pos<noms.length-1) {
				System.out.print(noms[pos].charAt(0));
				pos++;
			}
			System.out.print(noms[noms.length-1].charAt(0));
			String[] noms2= noms[noms.length-1].split(" I ");
			System.out.print(noms2[noms2.length-1].charAt(0));
			casos--;
			System.out.println();
		}
		
		
	}

}
