package Arrays;

import java.util.Scanner;

public class TotsDiferents2 {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int N= src.nextInt();
		for (int i=0; i<N;i++) {
			int K = src.nextInt();
			int[] larray = new int[K];
			int[] larray2 = new int[K];
			for(int s=0;s<K;s++) {
				larray[s]=src.nextInt();
			}
			for(int s2=0;s2<K;s2++) {
				larray2[s2]=src.nextInt();
			}
			Boolean comp=true;
			for(int x=0;x<K;x++) {
				for(int x2=0;x2<K;x2++) {
					if (larray[x]==larray2[x2]) {
						comp=false;
					}
				}
			}
			if(comp==true) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
		}

	}

}
