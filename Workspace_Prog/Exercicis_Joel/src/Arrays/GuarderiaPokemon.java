package Arrays;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class GuarderiaPokemon {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		Boolean sortir= false;
		ArrayList<String> poke = new ArrayList<String>();
		do{
			String opcio=src.next();
			switch(opcio) {
			case("A"):
				String nom_poke = src.next();
				if (!poke.contains(nom_poke))
					poke.add(nom_poke);
				break;
			case("B"):
				String nom_poke2=src.next();
				if (poke.contains(nom_poke2))
					poke.remove(poke.indexOf(nom_poke2));
				break;
			case("C"):
				poke.clear(); //TODO Eliminar un ArrayList entero
				break;
			case("D"):
				sortir=true;
				break;
			}
		}while(!sortir); //TODO Crear un doWhile
		Collections.sort(poke);
		System.out.println(poke);
	}
}