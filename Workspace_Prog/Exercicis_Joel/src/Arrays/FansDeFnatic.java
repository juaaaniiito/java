package Arrays;

import java.util.Scanner;

public class FansDeFnatic {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		for (; casos > 0; casos--) {
			int N = src.nextInt();
			src.nextLine();
			int wins, loses;
			wins = loses = 0;
			for (int l=0; N > l; l++) {
				String[] larray = src.nextLine().split(" - ");
				for (int i = 0; i < 2; i++) {
					if (larray[i].equals("Fnatic") && i % 2 == 0)
						wins++;
					if (larray[i].equals("Fnatic") && i % 2 != 0)
						loses++;
				}
			}
			System.out.println(wins + " " + loses);
		}
	}
}