package Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DNI {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos= src.nextInt();
		src.nextLine();
		for(;casos>0;casos--) {
			ArrayList<String> lletresDNI = new ArrayList<>(Arrays.asList("T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E")); //TODO Crear una lista con valores dentro.
			String DNI=src.nextLine();
			String lletra = DNI.substring(DNI.length()-1, DNI.length());
			DNI=DNI.replace(lletra, "");
			int DNI_int= Integer.parseInt(DNI);
			if(lletra.equals(lletresDNI.get((DNI_int%23)))) {
				System.out.print("valid");
			} else {
				System.out.print("invalid");
			}
		}
	}

}
