package Arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class CambialoUnPoco {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos = src.nextInt();
		for (;casos>0;casos--) {
			int K= src.nextInt();
			ArrayList<Integer> larray = new ArrayList<Integer>();
			for (int i =0; i<K;i++) {
				larray.add(src.nextInt());
			}
			int P1 = src.nextInt();
			int P2 = src.nextInt();
			for(int i=0; i<K;i++) {
				if (larray.get(i)==P1) {
					larray.set(i,P2);
				}
			}
			for(int i=0; i<K;i++) {
				System.out.print(larray.get(i)+" ");
			}
		}
	}
}