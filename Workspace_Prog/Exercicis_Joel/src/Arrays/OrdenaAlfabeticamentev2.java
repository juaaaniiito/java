package Arrays;

import java.util.Scanner;

public class OrdenaAlfabeticamentev2 {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos=src.nextInt();
		src.nextLine();
		for(;casos>0;casos--) {
			String frase=src.nextLine();
			frase=frase.replaceAll(" ", "");
			/*
			 * char[] caracters = frase.toCharArray();
			 * Arrays.sort(caracters); En comptes de fer servir un sort, 
			 * anem buscant lletra per lletra.
			 */
			for(char lletra='a'; lletra<='z';lletra++) { //TODO Ordena alfabeticamente
				for (int ajuda=0;ajuda<frase.length();ajuda++) {
					if(lletra==frase.charAt(ajuda)) {
						System.out.print(frase.charAt(ajuda));
					}
				}
			}
		}
	}
}