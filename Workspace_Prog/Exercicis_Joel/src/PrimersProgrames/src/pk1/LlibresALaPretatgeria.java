package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class LlibresALaPretatgeria {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int N = src.nextInt();
		int M = src.nextInt();
		int K = src.nextInt();
		int resultat= K - (N * M);
		if (resultat<=0) {
			System.out.println(0);
		} else {
			System.out.println(resultat);
		}
		
	}

}
