package PrimersProgrames.src.pk1;

import java.util.Scanner;

/*
 * Autor: Juan José Rodríguez
 * Entrada: Són els segons, cada cas consisteix amb un número de l'1 al 86399
 * Sortida: Per cada cas es respondrà amb l'hora en format hh:mm:ss . Si l'hora, minut o segon no té 0 no la poseu
 */
public class DeSegonsAHora {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int seg = src.nextInt();
		int hor= seg/3600;
		int res_hor = seg%3600;
		int min= res_hor/60;
		int res_min = res_hor%60;
		System.out.println(hor + ":" + min + ":" + res_min);
		}

}
