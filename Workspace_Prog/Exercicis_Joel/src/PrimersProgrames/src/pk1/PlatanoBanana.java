package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class PlatanoBanana {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String opcio = src.nextLine();
		int cont_P = 0;
		int cont_B = 0;
		while(!opcio.equals("0")) {
			if (opcio.equals("B")) {
				cont_B++;
			} else if (opcio.equals("P")) {
				cont_P++;
			}
			opcio = src.nextLine();
		}
		if (cont_P>cont_B) {
			System.out.println("M'agraden els platans");
		} else if (cont_P<cont_B) {
			System.out.println("M'agraden les bananes");
		} else {
			System.out.println("No puc distingir entre un platan i una banana");
		}
		

	}

}
