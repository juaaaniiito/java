package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class SumaUnSegon {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int H = src.nextInt();
		int M = src.nextInt();
		int S = src.nextInt();
		S=S+1;
		if (S>59) {
			M=M+1;
			S=0;
		}
		if (M>59) {
			H=H+1;
			M=0;
			S=0;
		}
		if (H>23) {
			S=0;
			M=0;
			H=0;
		}
		String strH = Integer.toString(H);
		String strM = Integer.toString(M);
		String strS = Integer.toString(S);
		System.out.println(strH + " " + strM +" "+ strS);
		
	}

}
