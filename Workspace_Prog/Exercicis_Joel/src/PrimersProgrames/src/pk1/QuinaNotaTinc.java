package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class QuinaNotaTinc {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String notes = src.nextLine();
		String[] cadanum = notes.split(","); //Separar el string a una "llista" per cada ,
		String nota1 = cadanum[0];
		String nota2 = cadanum[1];
		String nota3 = cadanum[2];
		float nota1_float = Float.valueOf(nota1);
		float nota2_float = Float.valueOf(nota2);
		float nota3_float = Float.valueOf(nota3);
		float promig = (float) ((nota1_float*0.3) +(nota2_float*0.5)+(nota3_float*0.2));
		if (nota1_float<4|| nota2_float<4) {
			System.out.println("Suspes");
		} else {
			if(promig<5) {
				System.out.println("Suspes");
			} else if (promig>=5 && promig<=7) {
				System.out.println("Aprovat");
			} else if (promig>7 && promig<=9) {
				System.out.println("Notable");
			} else if (promig >9) {
				System.out.println("Excel·lent");
			}
		}
		
	}

}
