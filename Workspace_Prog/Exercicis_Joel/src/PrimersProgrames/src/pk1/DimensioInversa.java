package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class DimensioInversa {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		double num = src.nextDouble();
		double div = (double)1.0/num;
		System.out.println(div);
	}

}
