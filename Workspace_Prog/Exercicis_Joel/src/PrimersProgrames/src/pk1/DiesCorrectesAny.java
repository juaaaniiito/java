package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class DiesCorrectesAny {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int dies;
		dies = src.nextInt();
		if (dies<1 || dies>366) {
			System.out.println("Incorrecte!");
		} else if (dies>=1 && dies<=365) {
			System.out.println("Correcte per un any no bixest!");
		} else {
			System.out.println("Correcte per un any bixest!");
		}
			
	}

}
