package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class SouiIRPF {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String nom =src.nextLine();
		int edat =src.nextInt();
		float hores = src.nextFloat();
		float salari = (float) 0.0;
		float valor_irpf = (float) 0.0;
		float total_irpf = (float) 0.0;
		float mes = (float) 4.5;
		float hores_extra=(float)0.0;
		if (hores<40) {
			salari= ((hores*10)*mes);
			if ((edat>=35 && ((salari*14)>20000))&& edat<50) {
				valor_irpf = (float) 0.17;
				total_irpf = salari*valor_irpf;
			} else if ((edat>=50)) {
				valor_irpf = (float) 0.14;
				total_irpf = salari*valor_irpf;
			} else {
				valor_irpf = (float) 0.2;
				total_irpf = salari*valor_irpf;
			}
		}
		if (hores>=40) {
			hores_extra=(float)(hores-40);
			salari= (float) ((40.0*10)*mes);
			salari= salari +((hores_extra*15)*mes);
			if ((edat>=35 && ((salari*14)>20000))&& edat<50) {
				valor_irpf = (float) 0.17;
				total_irpf = salari*valor_irpf;
			} else if ((edat>=50)) {
				valor_irpf = (float) 0.14;
				total_irpf = salari*valor_irpf;
			} else {
				valor_irpf = (float) 0.2;
				total_irpf = salari*valor_irpf;
			}
		}
		
		System.out.println(nom +" te un salari de " + salari +" euros al mes i paga un irpf de "+ total_irpf +" euros.");

	}

}
