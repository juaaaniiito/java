package PrimersProgrames.src.pk1;
/*
 * Per cada cas es dira "Hola, nom", sent "nom" el nom entrat
 */
import java.util.Scanner;

public class HolaUser {

	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in); // Crea l'objecte de escàner
		String user = myObj.nextLine(); // Afegim el que entri a la variable
		System.out.println("Hola, "+ user);
	}

}