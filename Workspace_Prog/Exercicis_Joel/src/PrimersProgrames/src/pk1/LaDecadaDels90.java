package PrimersProgrames.src.pk1;

import java.util.Scanner;

public class LaDecadaDels90 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String N = src.nextLine();
		String numbers = N.substring(N.length() - 2);
		int numEnter = Integer.parseInt(numbers); //Pasem de un str a int
		if (numEnter>=90 && numEnter<=99) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}
	}

}
