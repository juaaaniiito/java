package Activitat4.src.Activitat4;

import java.util.Scanner;

public class Bucles1ProducteNumerosNaturals {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos= src.nextInt();
		while (casos>0) {
			int num= src.nextInt();
			int suma=0;
			int producte=1;
			if (num<=0) {
				System.out.println("ELS NOMBRES NATURALS COMENCEN EN 1");
			} else {
				int num_suma_aux=num;
				int num_prod_aux=num;
				while(num>0) {
					suma=suma+num_suma_aux;
					producte=producte*num_prod_aux;
					num_suma_aux--;
					num_prod_aux--;
					num--;
				}
				System.out.println("SUMA: "+ suma +" PRODUCTE: "+ producte);
			}
			casos--;
		}

	}

}
