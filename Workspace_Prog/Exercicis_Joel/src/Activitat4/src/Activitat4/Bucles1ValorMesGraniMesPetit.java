package Activitat4.src.Activitat4;

import java.util.Scanner;

public class Bucles1ValorMesGraniMesPetit {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int num = src.nextInt();
		int gran = num; // També és possible posar: int gran = Integer.MIN_VALUE
		int petit = num;  // També és possible posar: int petit = Integer.MAX_VALUE
		while(num!=0) {
			if (num>gran) {
				gran=num;
			}
			if (num<petit) {
				petit=num;
			}
			num = src.nextInt();
		}
		System.out.println(gran+ " "+ petit);
	}

}
