package Activitat4.src.Activitat4;

import java.util.Scanner;

public class ContarVocals {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		String frase;
		int a, e, i , o, u, pos, car_total;
		src.nextLine();
		while (casos>0) {
			frase = src.nextLine();
			a=e= i= o= u=pos=0;
			pos=0;
			frase= frase.toUpperCase();
			while (pos<frase.length()) {
				switch (frase.charAt(pos)) {
				case ('A'):
					a++;
					break;
				case ('E'):
					e++;
					break;
				case ('I'):
					i++;
					break;
				case ('O'):
					o++;
					break;
				case ('U'):
					u++;
					break;
				}
				pos++;
			}
			System.out.println("A: "+a+" E: "+e+" I: "+i+" O: "+o+" U: "+u);
			casos--;
		}
	}

}
