package Activitat4.src.Activitat4;

import java.util.Scanner;

public class Bucles1NumerosPositius {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		int positius = 0;
		while (casos>0) {
			int num = src.nextInt();
			if (num>0) {
				positius++;
			}
			casos--;
		}
		System.out.println(positius);

	}

}
