package Activitat4.src.Activitat4;

import java.util.Scanner;

public class TracaCovid {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos, dies, contagis, cont_dies, alarma;
		casos= src.nextInt();
		while(casos>0) {
			alarma=0;
			dies=src.nextInt();
			while(dies>0) {
				contagis=src.nextInt();
				if (contagis>=300) {
					alarma++;
				}
				dies--;
			}
			if (alarma>0) {
				System.out.println("ALARMA");
			} else {
				System.out.println("OK");
			}
			casos--;
		}

	}

}
