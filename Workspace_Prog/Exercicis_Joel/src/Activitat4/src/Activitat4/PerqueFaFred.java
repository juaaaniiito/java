package Activitat4.src.Activitat4;

import java.util.Scanner;

public class PerqueFaFred {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String graus = src.nextLine();
		String[] sep_graus = graus.split(" ");
		int cont = sep_graus.length;
		int cont_fred = 0;
		float suma_fred= (float) 0.0;
		while (cont>0) {
			int int_sep_graus = Integer.parseInt(sep_graus[cont-1]);
			if (int_sep_graus<0) {
				cont_fred++;
				suma_fred = suma_fred+int_sep_graus;
			} 
			cont--;
		}
		float res=(float) 0.0;
		res = suma_fred / cont_fred;
		System.out.println(cont_fred);
		System.out.println(res);
		
	}

}
