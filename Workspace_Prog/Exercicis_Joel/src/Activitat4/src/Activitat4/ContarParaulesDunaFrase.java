package Activitat4.src.Activitat4;

import java.util.Scanner;

public class ContarParaulesDunaFrase {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos= src.nextInt();
		String frase;
		int paraules, posicio;
		src.nextLine();
		while(casos>0) {
			paraules=0;
			frase=src.nextLine();
			posicio=1;
			while(posicio<frase.length()) {
				if (frase.charAt(posicio)==' '&&frase.charAt(posicio-1)!=' ') {
					paraules++;
				}
				posicio++;
			}
			if (frase.charAt(frase.length()-1)==' ') { // Ajustament per si el darrer caracter no és un espai
				System.out.println(paraules);
			} else {
				System.out.println(paraules+1);
			}
			casos--;
		}
	}

}