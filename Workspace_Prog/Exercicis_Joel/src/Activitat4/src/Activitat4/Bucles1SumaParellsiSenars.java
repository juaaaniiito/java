package Activitat4.src.Activitat4;

import java.util.Scanner;

public class Bucles1SumaParellsiSenars {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int casos = src.nextInt();
		int suma_parells = 0;
		int suma_senars = 0;
		while(casos>0) {
			int num = src.nextInt();
			while(num>0) {
				if ((num%2)==0) {
					suma_parells=suma_parells+num;
				} else {
					suma_senars=suma_senars+num;
				}
				num--;
			}
			System.out.println("PARELLS: "+ suma_parells + " SENARS: " +suma_senars);
			casos--;
			suma_parells=0;
			suma_senars=0;
		}
	}

}
