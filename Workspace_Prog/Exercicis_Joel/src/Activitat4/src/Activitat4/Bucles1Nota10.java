package Activitat4.src.Activitat4;

import java.util.Scanner;

public class Bucles1Nota10 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int nota=src.nextInt();
		int valides=0;
		int notes10=0;
		while (nota!=-1) {
			
			if (nota>=0 && nota<=10) {
				valides++;
				if (nota==10) {
					notes10++;
				}
			}
			nota = src.nextInt();
		}
		System.out.println("TOTAL NOTES: "+ valides + " NOTES10: "+ notes10);
	}

}
