package Activitat4.src.Activitat4;

import java.util.Scanner;

public class JordiWhile {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int cont_videos = 0;
		int strikes=0;
		while(strikes<3) {
			int view = src.nextInt();
			if (view!=-1) {
				cont_videos++;
			} else {
				strikes++;
			}
		}
		System.out.println(cont_videos);
	}

}
