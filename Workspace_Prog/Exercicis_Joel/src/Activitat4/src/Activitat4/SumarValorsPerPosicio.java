package Activitat4.src.Activitat4;

import java.util.Scanner;

public class SumarValorsPerPosicio {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos, casos2, num, suma, cont_aux;
		casos= src.nextInt();
		while(casos>0) {
			suma= 0;
			cont_aux=1;
			casos2=src.nextInt();
			while(casos2>cont_aux-1) {
				num=src.nextInt();
				if (cont_aux%3==0)
					suma=suma+num;
				cont_aux++;
			}
			System.out.println(suma);
			casos--;
		}

	}

}
