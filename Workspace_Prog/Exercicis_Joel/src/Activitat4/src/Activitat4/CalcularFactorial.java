package Activitat4.src.Activitat4;

import java.util.Scanner;

public class CalcularFactorial {

	public static void main(String[] args) {
		Scanner src=new Scanner(System.in);
		int casos=src.nextInt();
		while (casos>0) {
			long total=1;
			int num = src.nextInt();
			while(num>0) {
				total=total*(num);
				num--;
			}
			System.out.println(total);
			casos--;
		}

	}

}
