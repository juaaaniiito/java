package Activitat4.src.Activitat4;

import java.util.Scanner;

public class ElRadarDeLaValentina {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos =src.nextInt();
		int energia, cops_energia;
		Boolean m,b;
		while(casos>0) {
			cops_energia=0;
			m=b=false;
			while(cops_energia<5) {
				energia=src.nextInt();
				if (energia>=10000) {
					m=true;
				}else if(energia>=1000){
					b=true;
				}
				cops_energia++;
			}
			if (m==true) {
				System.out.println("M");
			} else if (b==true) {
				System.out.println("B");
			} else {
				System.out.println("H");
			}
			
			//Reiniciem els casos
			casos--;
		}

	}

}
