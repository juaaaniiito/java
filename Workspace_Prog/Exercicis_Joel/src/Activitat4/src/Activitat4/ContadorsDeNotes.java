package Activitat4.src.Activitat4;

import java.util.Scanner;

public class ContadorsDeNotes {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		float nota = src.nextInt();
		int valides = 0;
		float suma = 0;
		int cont_exc =0;
		int cont_not =0;
		int cont_bens =0;
		int cont_suf =0;
		int cont_ins =0;
		int cont_moltdef =0;
		while(nota!=-1) {
			if (nota>=0&&nota<=10) {
				valides++;
				suma=suma+nota;
				if (nota>=9) {
					cont_exc++;
				} else if (nota >=7&& nota <9) {
					cont_not++;
				} else if (nota >=6&& nota <7) {
					cont_bens++;
				} else if (nota >=5 && nota <6) {
					cont_suf++;
				} else if (nota >3 && nota < 5) {
					cont_ins++;
				} else if (nota >=0 && nota <=3) {
					cont_moltdef++;
				}
			}
			nota = src.nextInt();
		}
		float mitjana = suma/valides;
		System.out.println("NOTES: "+ valides+" MITJANA: "+mitjana+" E: "+cont_exc+ " N: "+ cont_not+ " B: "+ cont_bens+" S: "+cont_suf+" I: "+cont_ins+ " MD: "+ cont_moltdef);
	}

}
