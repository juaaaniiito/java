package pk1;

import java.util.Scanner;

public class PrimerDigit {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String A = src.nextLine();
		String B = src.nextLine();
		String primerA = A.substring(0,1);
		String primerB = B.substring(0,1);
		int intA = Integer.parseInt(primerA);
		int intB = Integer.parseInt(primerB);
		if (intA>intB) {
			System.out.println("A");
		} else if (intA<intB) {
			System.out.println("B");
		}
	}

}
