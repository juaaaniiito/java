package pk1;

import java.util.Scanner;

public class Quadrant {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int X = src.nextInt();
		int Y = src.nextInt();
		
		if (X>0) {
			if (Y>0) {
				System.out.println("1");
			} else if (Y==0) {
				System.out.println("1,4");
			} else {
				System.out.println("4");
			}
		} else if (X<0){
			if (Y>0) {
				System.out.println("2");
			} else if (Y==0) {
				System.out.println("2,3");
			} else {
				System.out.println("3");
			}
		} else{ // else if (X==0) 
			if (Y>0) {
				System.out.println("1,2");
			} else if (Y==0) {
				System.out.println("1,2,3,4");
			} else {
				System.out.println("3,4");
			}
		}

	}

}
