package pk1;

import java.util.Scanner;

public class Canonades {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		double metres_double= src.nextDouble();
		double de_4 = metres_double-25;
		double de_5 = metres_double-75;
		double total = 0;
		if (de_5>0) {
			total= total+(de_5*5);
		}
		if (de_4>0 && metres_double<=75) {
			total=total+(de_4*4);
		} else if (de_4>0 && metres_double>75) {
			total=total+(50*4);
		}
		if (metres_double>25) {
			total=total+(25*3);
		} else {
			total=total+(metres_double*3);
		}
		System.out.println(metres_double + "m: " + total + " euros");
	}
}