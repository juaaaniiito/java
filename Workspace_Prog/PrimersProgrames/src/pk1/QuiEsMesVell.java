package pk1;

import java.util.Scanner;

public class QuiEsMesVell {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String nom1 = src.next();
		int edat1 = src.nextInt();
		String nom2 = src.next();
		int edat2 = src.nextInt();
		if (edat1>edat2) {
			System.out.println(nom1);
		} else if (edat1<edat2) {
			System.out.println(nom2);
		} else {
			System.out.println("Cap és més vell");
		}

	}

}
