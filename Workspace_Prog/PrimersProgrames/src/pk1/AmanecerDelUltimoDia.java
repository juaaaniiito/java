package pk1;

import java.util.Scanner;

public class AmanecerDelUltimoDia {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		int segons= src.nextInt();
		int mati_nit = segons%86400;
		int dia = segons/86400;
		dia =dia+1;
		if (mati_nit<43200) {
			System.out.println("mati del dia "+dia);
		} else {
			System.out.println("nit del dia "+dia);
		}
	}

}
