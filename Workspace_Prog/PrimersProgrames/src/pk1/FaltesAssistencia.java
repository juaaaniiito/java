package pk1;

import java.util.Scanner;

public class FaltesAssistencia {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String Text = src.nextLine();
		String[] cada = Text.split(" "); //Separar el string a una "llista"
		int numEnter0 = Integer.parseInt(cada[0]); // Pasar a integer un valor en espcífic de la "llista" anterior.
		int numEnter1 = Integer.parseInt(cada[1]);
		int numEnter2 = Integer.parseInt(cada[2]);
		if (numEnter2>(numEnter0*0.1)) {
			System.out.println("NO");
		} else if ((numEnter1+numEnter2)>(numEnter0*0.2)) {
			System.out.println("NO");
		} else {
			System.out.println("SI");
		}
		
	}

}