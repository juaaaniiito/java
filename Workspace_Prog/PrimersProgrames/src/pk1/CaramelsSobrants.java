package pk1;

import java.util.Scanner;

/*
 * Autor: Juan José Rodríguez
 * Entrada: primera nombre caramels i segona nombre nens
 */
public class CaramelsSobrants {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int caramels = src.nextInt();
		int nens = src.nextInt();
		int sobrants = caramels%nens;
		System.out.println(sobrants);

	}

}
