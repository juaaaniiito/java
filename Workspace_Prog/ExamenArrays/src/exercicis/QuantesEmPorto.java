package exercicis;

import java.util.Scanner;

public class QuantesEmPorto {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String num1 = src.next();
		String num2 = src.next();
		while (!num1.equals("0") && !num2.equals("0")) { // Quan sigui '0 0' s'acaben els casos de prova.
			int acarreo = 0;
			for (int i = 0; i < num1.length(); i++)
				if (Integer.parseInt(num1.substring(i, i + 1)) + Integer.parseInt(num2.substring(i, i + 1)) >= 10) // Quan la suma dels dígits sigui major a 9, sumar al contador
					acarreo++;
			System.out.println(acarreo);
			num1 = src.next();
			num2 = src.next();
		}
	}
}