package exercicis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ElPuzzle {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int num_peces = src.nextInt();
		while (num_peces != 0) {
			src.nextLine();
			String peces = src.nextLine();
			ArrayList<String> llista_peces = new ArrayList<>(Arrays.asList(peces.split(" ")));//Omplim la lista amb el contingut que ha entrat
			int ajuda = 1;
			for (int i = 0; i < num_peces; i++) { //Recorrem la llista comparant els numeros de l'1 fins a 'num_peces'
				if (!llista_peces.contains(String.valueOf(ajuda))) { // llista_peces.size()+(num_peces-llista_peces.size())
					System.out.print(ajuda + " ");
					i++;
				}
				ajuda++;
			}
			num_peces = src.nextInt();
		}

	}

}
