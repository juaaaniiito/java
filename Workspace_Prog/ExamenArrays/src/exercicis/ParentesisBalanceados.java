package exercicis;

import java.util.Scanner;

public class ParentesisBalanceados {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String frase = src.nextLine();
		while (frase.length() != 0) {
			int cont1 = 0;
			int cont2 = 0;
			int cont3 = 0;
			Boolean malament= false;
			for (int i = 0; i < frase.length(); i++) {
				switch(frase.charAt(i)) {
				case ('('): cont1++; break;
				case (')'): cont1--; break;
				case ('['): cont2++; break;
				case (']'): cont2--; break;
				case ('{'): cont3++; break;
				case ('}'): cont3--; break;
				}
				if (cont1<0 || cont2<0 || cont3<0)
					malament=true;
				
			}
			if((cont1==0 && cont2==0 && cont3==0) && !malament)
				System.out.println("YES");
			else
				System.out.println("NO");
			frase = src.nextLine();
		}
	}
}