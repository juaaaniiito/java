package ExamenWhile;

import java.util.Scanner;

public class LosSaltosDeMario {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int salts=src.nextInt();
		int seguent_pis, pis_actual, salts_dalt, salts_baix;
		salts_dalt=salts_baix=0;
		pis_actual=src.nextInt();
		while(salts>1) { //Recòrrer el bucle el numero de vegades que fa un salt.
			seguent_pis=src.nextInt();
			if (pis_actual>seguent_pis) { //Decidim si el salt és cap a dalt, o cap abaix i ho sumem al contador corresponent.(Si hi ha salt)
				salts_baix++;
			} else if(pis_actual<seguent_pis) {
				salts_dalt++;
			}
			pis_actual=seguent_pis;
			salts--;
		}
		System.out.println(salts_dalt+ " "+ salts_baix); //Indiquem el numero de salts
	}

}
