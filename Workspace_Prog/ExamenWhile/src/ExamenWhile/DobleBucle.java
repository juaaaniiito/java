package ExamenWhile;

import java.util.Scanner;

public class DobleBucle {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int num =src.nextInt();
		/*
		Boolean num_valid=false;
		while(num_valid==false) { //Quan el nombre sigui vàlid, passem al següent bucle
			num= src.nextInt();//Demanem el nombre
			if (num>0) { // Quan sigui vàlid, canviem el valor de la variable
				num_valid=true;
			} else {
				System.out.println("Error, número no vàlid. Introdueix un altre nombre positiu."); 
			}
		}
		*/
		while(num<=0) {
			System.out.println("Error, número no vàlid. Introdueix un altre nombre positiu.");
			num=src.nextInt();
		}
		int posicio=1;
		while(posicio<num+1) {
			int aux=posicio;
			while(aux>0) {
				System.out.print(posicio);
				aux--;
			}
			posicio++;
		}
	}

}

