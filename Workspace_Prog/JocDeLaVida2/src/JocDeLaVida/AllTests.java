package JocDeLaVida;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
/**
 * Suite que executa les classes de Tests.
 * @author Juan Jose Rodriguez
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ actualitzarTaulerTest.class, evolucionaTest.class })
public class AllTests {

}
