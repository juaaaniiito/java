package Activitat4;

import java.util.Scanner;

public class Bucles1SumaPositiusiNegatius {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int num = src.nextInt();
		int pos= 0;
		int neg= 0;
		while(num!=0) {
			if (num>0) {
				pos++;
			} else {
				neg++;
			}
			num = src.nextInt();
		}
		if (pos>neg) {
			System.out.println("POSITIUS");
		} else if (pos<neg) {
			System.out.println("NEGATIUS");
		} else {
			System.out.println("IGUALS");
		}
	}

}
