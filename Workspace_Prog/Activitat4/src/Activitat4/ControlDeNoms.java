package Activitat4;

import java.util.Scanner;

public class ControlDeNoms {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String paraula = src.nextLine();
		char carac_ref, carac_canv;
		carac_ref= paraula.charAt(0);
		Boolean dif=false;
		int pos =1;
		while (pos<paraula.length()&&dif==false) {
			carac_canv=paraula.charAt(pos);
			if (carac_ref!= carac_canv) {
				dif=true;
			}
			pos++;
			
		}
		if (dif==true) {
			System.out.println("SI");
		} else {
			System.out.println("NO");
		}		
	}

}
