package Activitat4;

import java.util.Scanner;

public class ElMeuPrimerBucle {

	public static void main(String[] args) {
		Scanner src = new Scanner (System.in);
        int casos;
        int num;
        casos = src.nextInt();
        while (casos>0) {
            num=src.nextInt();
            System.out.println(num + 1);
            casos--;
        }
	}
}