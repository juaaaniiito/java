package Activitat4;

import java.util.Scanner;

public class ExplosionArcana1 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int dany_inicial, dany_final, explosions;
		dany_inicial= src.nextInt();
		explosions= src.nextInt();
		dany_final=0;
		int multiplicador = 1;
		while(explosions>0) {
			dany_final=dany_final+(dany_inicial*multiplicador);
			explosions--;
			multiplicador++;
		}
		System.out.println(dany_final);
	}

}
