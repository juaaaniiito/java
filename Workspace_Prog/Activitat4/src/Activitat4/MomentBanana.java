package Activitat4;

import java.util.Scanner;

public class MomentBanana {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		int vida, dany;
		float per_vida;
		while (casos>0) {
			vida = src.nextInt();
			dany = src.nextInt();
			per_vida = (float) (vida*0.2);
			
			if (dany>per_vida && dany <vida) {
				System.out.println("Momento Banana");
			} else if (dany >=vida) {
				System.out.println("Nope");
			} else if (dany<=per_vida){
				System.out.println("Nope");
			}
			
			//restem casos
			casos--;
		}

	}

}