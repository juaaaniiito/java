package Activitat4;

import java.util.Scanner;

public class Top50 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int pos = src.nextInt();
		int torneigs = src.nextInt();
		while (torneigs>0) {
			pos=pos/2;
			torneigs--;
		}
		System.out.println(pos);
	}

}
