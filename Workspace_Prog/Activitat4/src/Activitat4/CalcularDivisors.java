package Activitat4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CalcularDivisors {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		int n, ajuda, ajuda_print;
		List<Integer> divisors = new ArrayList<>();
		while (casos>0) {
			divisors.clear();
			n=src.nextInt();
			/* AQUI ESTA HACIENDOLO SUMANDO
			ajuda=1;
			while (ajuda<n+1) {
				if ((n%ajuda)==0) {
					divisors.add(ajuda);
				}
				ajuda++;
			}
			*/
			//AQUI ESTA HACIENDOLO HACIA ATRÁS, PARA HACER EL PRINT MÁS FÁCIL
			ajuda=n;
			while (ajuda>0) {
				if ((n%ajuda)==0) {
					divisors.add(ajuda);
				}
				ajuda--;
			}
			casos--;
			ajuda_print = divisors.size();
			StringBuilder print = new StringBuilder();
			while(ajuda_print>0) {
				print=print.append(divisors.get(ajuda_print-1)+" ");
				ajuda_print--;
			}
			System.out.println(print);
		}

	}
}
