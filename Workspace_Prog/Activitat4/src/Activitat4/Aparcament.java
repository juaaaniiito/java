package Activitat4;

import java.util.Scanner;

public class Aparcament {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String espais; 
		String[] espais_sep;
		int inici_espai, final_espai, anterior, longitud_cotxe;
		Boolean hihalloc;
		longitud_cotxe= src.nextInt();
		while(longitud_cotxe!=0) {
			src.nextLine();
			espais = src.nextLine();
			int lloc=1;
			int num_lloc=0;
			hihalloc=false;
			anterior = Integer.MAX_VALUE;
			while (!espais.equals("0")) {
				
				espais_sep= espais.split(" ");
				inici_espai = Integer.parseInt(espais_sep[0]);
				final_espai = Integer.parseInt(espais_sep[1]);
				
				if (longitud_cotxe*1.5 <= final_espai-inici_espai&& final_espai-inici_espai<anterior) {
					num_lloc= lloc;
					anterior= final_espai-inici_espai;
					hihalloc= true;
				}
				lloc++;
				
				//Reiniciem la variable espais
				espais=src.nextLine();
			}
			
			if (hihalloc==true) {
				System.out.println("SI "+ num_lloc);
			} else {
				System.out.println("NO");
			}
			longitud_cotxe = src.nextInt();
			
		}
	}

}
