package Activitat4;

import java.util.Scanner;

	public class JordiWhile2 {
		public static void main(String[] args) {
			Scanner src = new Scanner(System.in);
			int strikes=0;
			int views=0;
			while(strikes<3) {
				int view = src.nextInt();
				if (view!=-1) {
					views=views+view;
				} else {
					strikes++;
				}
			}
			System.out.println(views);
		}
}
