package examen1;

import java.util.Scanner;

public class exercici4 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		String codi = src.nextLine();
		String provincia = codi.substring(0,2);// Agafem els dos primers numeros per determinar la provincia
		String zona_post = codi.substring(2); // Agafem la resta de numeros per determinar la zona
		String provincia_fn = "";
		String zona_post_fn = "";
		switch (provincia) { //Distinguim i assignem les províncies
		case("08"):
			provincia_fn="Barcelona";
			break;
		case("43"):
			provincia_fn="Tarragona";
			break;
		case("25"):
			provincia_fn="Lleida";
			break;
		case("17"):
			provincia_fn="Girona";
			break;
		default:
			provincia_fn="Província errònia";
		}
		if (provincia_fn.equals("Província errònia")) { // Si la província età malament, ho posem directament
			System.out.println(provincia_fn); 
		} else {
			switch (zona_post) { //Distinguim i assignem les zones
			case ("080"):
				zona_post_fn="Apartats postals";
				break;
			case ("070"):
				zona_post_fn="Correspondencia postal o telegràfica.";
				break;
			case("071"):
				zona_post_fn="Organismes oficials";
				break;
			default:
				zona_post_fn="Zona postal estàndard";
			}
			System.out.println(provincia_fn);// Mostrem la provincia
			System.out.println(zona_post_fn);// Mostrem la zona
		}
	}

}
