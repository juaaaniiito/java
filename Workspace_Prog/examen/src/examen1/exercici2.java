package examen1;

import java.util.Scanner;

public class exercici2 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in); //Dice que se tenia q poner un input x cada dia y mes y de todo.
		String data1 = src.nextLine(); // Llegim la primera data
		String data2 = src.nextLine(); // Llegim la segona data
		String dia1 = data1.substring(0,2); //Separem el dia de la primera data
		String mes1 = data1.substring(3,5); // Separem el mes de la primera data
		String any1 = data1.substring(6); // Separem l'any de la primera data
		String dia2 = data2.substring(0,2); // Separem el dia de la segona data
		String mes2 = data2.substring(3,5); // Separem el mes de la segona data
		String any2 = data2.substring(6); // Separem l'any de la segona data
		// Ara passem els strings a int per treballar amb ells
		int dia1_int = Integer.parseInt(dia1);
		int mes1_int = Integer.parseInt(mes1);
		int any1_int = Integer.parseInt(any1);
		int dia2_int = Integer.parseInt(dia2);
		int mes2_int = Integer.parseInt(mes2);
		int any2_int = Integer.parseInt(any2);
		/*
		 * 
		 */
		int dif_anys = any2_int - any1_int;
		int dif_mesos = mes2_int - mes1_int;
		int dif_dies = dia2_int - dia1_int;
		if (dif_anys>=0 && dif_mesos>=0 && dif_dies>=0) {
			System.out.println(dif_dies + " dies " + dif_mesos + " mesos " + dif_anys + " anys");
		} else if (dif_anys>=0 && dif_mesos<=0) {
			if (dia1_int>31) {
				dia1_int= 0;
				mes1_int= mes1_int+1;
			}
		}
		
	}

}