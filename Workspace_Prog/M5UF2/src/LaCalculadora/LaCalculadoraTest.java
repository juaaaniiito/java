package LaCalculadora;

import static org.junit.Assert.*;

import org.junit.Test;

public class LaCalculadoraTest {

	@Test
	public void testSuma() {
		assertEquals(LaCalculadora.suma(10, 20), 30);
		//fail("Not yet implemented");
	}

	@Test
	public void testResta() {
		assertEquals(LaCalculadora.resta(5, 2),3);
		//fail("Not yet implemented");
	}

	@Test
	public void testMultiplicacio() {
		assertEquals(LaCalculadora.multiplicacio(2, 8), 16);
		//fail("Not yet implemented");
	}

	@Test
	public void testDivisio() {
		assertEquals(LaCalculadora.divideix(10, 2), 5);
		//fail("Not yet implemented");
	}
	
	@Test (expected = java.lang.ArithmeticException.class)
	public void testDivideix() {
		int res = LaCalculadora.divideix(10, 0);
		assertEquals("Ha fallat el método divideix", 2,  res);
	}

	@Test
	public void testException() {
		int res;
		try {
			res = LaCalculadora.divideix(10, 0);
			fail("Fallo: Passa per aquí si no es llança l’excepció ArithmeticException");
		}
		catch (ArithmeticException e) {
			//La prova funciona correctament
		}
	}
	
}
