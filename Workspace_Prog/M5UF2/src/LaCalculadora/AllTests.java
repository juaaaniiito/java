package LaCalculadora;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LaCalculadoraTestDivisio.class, LaCalculadoraTestMultiplicacio.class, LaCalculadoraTestResta.class,
		LaCalculadoraTestSuma.class })
public class AllTests {

}
