package Exercicis;

public class Exercici6 {

	public static void main(String[] args) {
		
		int[] VectorNums = {7 , 4, 3 , 6};
		System.out.println("La suma dels nombres del vector és: " + SumarVector(VectorNums, VectorNums.length-1));

	}

	private static int SumarVector(int[] vectorNums, int i) {
		
		int resultat = 0;
		if (i == 0) resultat = vectorNums[0];
		else resultat = vectorNums[i] + SumarVector(vectorNums, i-1);
		
		return resultat;
	}

}
