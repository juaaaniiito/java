package Exercicis;

import java.util.Scanner;

public class Exercici9 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.print("Introdueix el primer nombre: ");
		int num1 = src.nextInt();
		System.out.print("\nIntrodueix el segon nombre: ");
		int num2 = src.nextInt();
		if (num1>num2)
			System.out.println("\nEl producte és: " + CalcularProducte(num1,num2));
		else
			System.out.println("\nEl producte és: " + CalcularProducte(num2,num1));
	}

	private static int CalcularProducte(int num1, int num2) {
		int resultat;
		if (num2==1)
			return num1;
		if (num2%2!=0)
			resultat = num1 + CalcularProducte(num1*2, num2/2);
		else
			resultat = CalcularProducte(num1*2, num2/2);
		return resultat;
	}

}
