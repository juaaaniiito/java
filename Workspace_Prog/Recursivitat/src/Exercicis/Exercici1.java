package Exercicis;

import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Introdueix un numero per calcular el seu factorial: ");
		int n = src.nextInt();
		System.out.println("El factorial de " + n + " és: " + CalcularFactorial(n));
	}

	private static long CalcularFactorial(int n) {
		long resultat=0;
		if (n==1 || n==0) {
			resultat=1;
			return resultat;	
		}
		resultat = n * CalcularFactorial(n-1);
		
		return resultat;
	}

}
