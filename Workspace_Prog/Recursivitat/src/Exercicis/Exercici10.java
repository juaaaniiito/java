package Exercicis;

import java.util.Scanner;

public class Exercici10 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Introdueix un nombre en binari:");
		long num1 = src.nextLong();
		int ajuda =0;
		System.out.println("El nombre " + num1 + " en decimal és: " + CalcularDecimal(num1, ajuda));

	}

	private static double CalcularDecimal(long num1, long ajuda) {
		double resultat = 0;
		if (num1<10)
			return Math.pow(2, ajuda);
		if (num1%10 == 1)
			resultat = Math.pow(2, ajuda) + CalcularDecimal(num1/10,ajuda+1);
		if (num1%10 == 0)
			resultat = CalcularDecimal(num1/10,ajuda+1);
		return resultat;
	}

}
