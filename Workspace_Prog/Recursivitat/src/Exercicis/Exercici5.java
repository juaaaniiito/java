package Exercicis;

import java.util.Scanner;

public class Exercici5 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Entra un nombre: ");
		int n = src.nextInt();
		System.out.println("El resultat és: " + SumaDigits(n));
		
		
		
	}

	private static int SumaDigits(int n) {
		int resultat = 0;
		if (n<10) resultat = n;
		else
			resultat = n % 10 + SumaDigits(n/10);
		
		return resultat;
	}

}
