package Exercicis;

import java.util.Scanner;

	public class Exercici4 {
		public static void main(String[] args) {
			Scanner src = new Scanner(System.in);
			long n = 0;
			System.out.println("La suma dels 100 primers nombres naturals parells és: " + CalcularResultat(n));
	}

		private static long CalcularResultat(long n) {
			long resultat = 0;
			if (n>100) return resultat;
			else {
				if (n%2==0) {
				resultat = n + (CalcularResultat(n+1));
				}
				else
					resultat = CalcularResultat(n+1);
			}
			
			return resultat;
		}
}