package Exercicis;

import java.util.Scanner;

public class Exercici3 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Entra el dividend: ");
		int dividend = src.nextInt();
		System.out.println("Entra el divisor: ");
		int divisor = src.nextInt();
		System.out.println("El resultat és: " + DivisioEntera(dividend, divisor));
	}

	private static int DivisioEntera(int dividend, int divisor) {
		int resultat = 0;
		if (dividend < divisor) return resultat;
		return resultat=1 + DivisioEntera(dividend - divisor, divisor);
	}
}