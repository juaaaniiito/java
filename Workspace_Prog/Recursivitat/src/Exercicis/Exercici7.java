package Exercicis;

import java.util.Scanner;

public class Exercici7 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Escriu un nombre: ");
		int n = src.nextInt();
		double n2 = n;
		System.out.println("Escriu l'exponent: ");
		int x = src.nextInt();
		System.out.println("El resultat és: " + CalcularPotencia(n2, x));
	}

	private static double CalcularPotencia(double n, int x) {
		double resultat;
		if (x<0) {
			n = (1/n);
			x = Math.abs(x);
		}
		if (x>1)
			resultat = n * CalcularPotencia(n, x-1);
		else
			resultat = n;
		return resultat;
	}

}
