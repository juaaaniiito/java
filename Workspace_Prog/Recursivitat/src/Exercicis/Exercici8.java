package Exercicis;

import java.util.Scanner;

public class Exercici8 {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Introdueix el primer valor: ");
		double num1 = src.nextDouble();
		System.out.println("Introdueix el segon valor: ");
		double num2 = src.nextDouble();
		if (num1>num2)
			System.out.println("El MCD dels dos valors és: " + CalcularMCD(num1, num2));
		else
			System.out.println("El MCD dels dos valors és: " + CalcularMCD(num2, num1));
	}

	private static double CalcularMCD(double num1, double num2) {
		double resultat;
		if (num1%num2 == 0) {
			return num2;
		}
		resultat = CalcularMCD(num2, num1%num2);
		return resultat;
	}

}
