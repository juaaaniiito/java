package Exercicis;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercici11 {
	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Introdueix el numero en decimal:");
		int num = src.nextInt();
		ArrayList<Integer> esc = new ArrayList<Integer>();
		System.out.print("\nEl numero decimal " + num + " en octal és: ");
		CalcularOctal(num, esc);
		for (int i = esc.size()-1; i>-1;i--)
			System.out.print(esc.get(i));
	}

	private static void CalcularOctal(int num, ArrayList<Integer> esc) {
		int resultat;
		if (num<8)
			esc.add(num);
		else {
			esc.add(num%8);
			CalcularOctal(num/8, esc);
		}
	}
}
