package JocDeLaVida;

import java.util.Random;
import java.util.Scanner;
/**
 * Programa desenvolupa un automat celular, on tenint una serie de normes, va evolucionant.
 * @author Juan Jose Rodriguez
 *
 */
public class Programa {

	static Scanner src = new Scanner(System.in);
	static Random rd = new Random();
	static int files = 0; //Faig les variables files i columnes globals per fer-les servir en els diferents mètodes.
	static int columnes = 0;
	
	public static void main(String[] args) {
		boolean acabat = false;
		boolean inicialitzat = false;
		char[][] tauler = new char[0][0];
		char opcio;
		do {
			opcio = mostrarMenu();
			switch (opcio) {
			case('1'):
				tauler = inicialitzarTauler();
				inicialitzat = true;
				break;
			case('2'):
				if (!inicialitzat)
					System.out.println("Primer has de inicialitzar el tauler(Opció 1).");
				else
					visualitzarTauler(tauler);
				break;
			case('3'):
				if (!inicialitzat)
					System.out.println("Primer has de inicialitzar el tauler(Opció 1).");
				else
					iterarTauler(tauler);
				break;
			case('0'):
				acabat=true;
			}
		}while(!acabat);
		System.out.println("Desapareixent cèl·lules...");
	}
/**
 * Pregunta a l'usuari quantes iteracions s'han de realitzar, i les realitza.
 * @param tauler Tauler principal del joc.
 */
	private static void iterarTauler(char[][] tauler) {
		visualitzarTauler(tauler);
		System.out.print("Introdueix el número d'iteracions que vols realitzar sobre el tauler: ");
		int iteracions = comprovarIteracions();
		for (int i=1; i<iteracions+1;i++) {
			char[][] tauler_nou = evoluciona(tauler);
			System.out.println("\nTauler iterat " + i + " vegade(s)");
			tauler = actualitzarTauler(tauler_nou);
			visualitzarTauler(tauler);
		}
	}
/**
 * Realitza una clonacio entre el tauler nou que ha creat i el principal.
 * @param tauler_nou Tauler des del qual realitzarem la clonacio.
 * @return Retorna el tauler principal clonat.
 */
	private static char[][] actualitzarTauler(char[][] tauler_nou) {
		char[][] tauler = new char[tauler_nou.length][tauler_nou[0].length];
		for (int i = 0; i<tauler_nou.length; i++) { 
			for (int j = 0; j<tauler_nou[0].length; j++) { 
				tauler[i][j] = tauler_nou[i][j];
			}
		}
		return tauler;
	}
/**
 * Conjunt de regles que determinen si una casella ha de canviar d'estat o no.
 * @param tauler Tauler principal del joc que serveix per comparar les caselles.
 * @return Retorna un nou tauler on ja s'han aplicat les regles.
 */
	private static char[][] evoluciona(char[][] tauler) {
		char[][] tauler_nou = new char[files][columnes];
		for (int i = 0; i<files; i++) {
			for (int j = 0; j<columnes; j++) {
				int celulesVoltant_viu=-1;
				int celulesVoltant_mort=0;
				for (int k = -1; k<2;k++) {
					for (int l = -1; l<2; l++) {
						if (dintreTauler(i-k, j-l)) {
							if (tauler[i][j]=='▓' && tauler[i-k][j-l]=='▓') {
								celulesVoltant_viu++;
							}
							if (tauler[i][j]=='░' && tauler[i-k][j-l]=='▓') {
								celulesVoltant_mort++;
							}
						}
					}
				}
				if (tauler[i][j]=='▓' &&(celulesVoltant_viu==2 || celulesVoltant_viu==3)) {
					tauler_nou[i][j]='▓';
				} else {
					tauler_nou[i][j]='░';
				}
				
				if (tauler[i][j] =='░' && celulesVoltant_mort==3) {
					tauler_nou[i][j]='▓';
				}
			}
		}
		return tauler_nou;
	}
/**
 * Comprova que la casella per comprovar estigui dins del tauler.
 * @param i Fila actual de la casella que estem comprovant.
 * @param j Columna actual de la casella que estem comprovant.
 * @return Valor boole que determina si est dins o no.
 */
	private static boolean dintreTauler(int i, int j) {
		boolean dintre=false;
		if (i>=0 && i<files && j>=0 && j<columnes)
			dintre=true;
		return dintre;
	}
/**
 * Certifiquem el numero de iteracions que entran per parametre.
 * @return Retorna aquest numero de iteracions.
 */
	private static int comprovarIteracions() {
		int iteracions=-1;
		boolean numValid = false;
		do {
			try {
				iteracions=src.nextInt();
				src.nextLine();
				numValid=true;
			} catch(Exception e) {
				src.nextLine();
				System.out.print("\nIntrodueix un valor enter: ");
			}
			if (iteracions<0 || iteracions>100)
				System.out.print("\nIntrodueix un valor entre 0 i 100: ");
		}while(!numValid || iteracions <0 || iteracions>100 );
		return iteracions;
	}
/**
 * Mostra el tauler per pantalla.
 * @param tauler Tauler principal del joc.
 */
	private static void visualitzarTauler(char[][] tauler) {
		System.out.println("AUTÒMAT CEL·LULAR:\n");
		for (int i =0; i<files; i++) {
			for (int j = 0; j<columnes; j++) {
				System.out.print(tauler[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("Cèl·lules mortes: ░");
		System.out.println("Cèl·lules vives: ▓");
	}
/**
 * Dimensiona el tauler, aixi com ficar les celules vives de manera aleatoria.
 * @return Retorna el tauler principal ja inicialitzat
 */
	private static char[][] inicialitzarTauler() {
		char[][] tauler = new char[0][0];
		System.out.print("Introdueix el numero de files(Entre 4 i 10): ");
		files = demanarDimensions();
		System.out.print("\nIntrodueix el numero de columnes(Entre 4 i 10): ");
		columnes = demanarDimensions();
		tauler = dimensionarTauler(tauler);
		return tauler;
	}
/**
 * Omple el tauler de celules mortes i fa un sorteig de quantes vives haura de ficar.
 * @param tauler Tauler principal del joc en el qual s'escriuran els nous valors.
 * @return Retorna el tauler dimensionat amb valors.
 */
	private static char[][] dimensionarTauler(char[][] tauler) {
		
		tauler= new char[files][columnes];
		for (int i =0; i<files; i++)
			for (int j = 0; j<columnes; j++)
				tauler[i][j]='░';
		int numCelules = rd.nextInt((files*columnes)/4, (files*columnes)/2);
		tauler = ficarCelules(numCelules, tauler);
		return tauler;
	}

	/**
	 * Fica les celules vives al tauler de manera aleatoria.
	 * @param numCelules Nombre de celules vives restants per ficar.
	 * @param tauler 
	 * @return Retorna el tauler dimensionat amb valors.
	 */
	private static char[][] ficarCelules(int numCelules, char[][] tauler) {
		while(numCelules>0) {
			int fila = rd.nextInt(0, files);
			int columna = rd.nextInt(0, columnes);
			if (tauler[fila][columna]!='▓') {
				tauler[fila][columna]='▓';
				numCelules--;
			} 
		}
		return tauler;
		
	}

	private static int demanarDimensions() {
		boolean numValid= false;
		int num=-1;
		do {
			try {
				num=src.nextInt();
				src.nextLine();
				numValid=true;
			} catch (Exception e) {
				System.out.println("Error, has d'introduir un valor enter.");
				src.nextLine();
			}
			if (num<4 || num>10 ) {
				System.out.print("\nIntrodueix un valor vàlid (Entre 4 i 10): ");
			}
		}while(!numValid || num<4||num>10);
		return num;
	}

	private static char mostrarMenu() {
		char opcio='l';
		boolean valid=false;
		do {
			System.out.println("\nMenú del Joc de la Vida\n");
			System.out.println("Escull una opció: ");
			System.out.println("1. Inicialitzar tauler.");
			System.out.println("2. Visualitzar tauler.");
			System.out.println("3. Iterar.");
			System.out.println("0. Sortir.");
			System.out.print("\nEscull una opció: ");
			opcio = src.next().charAt(0);
			if (opcio=='1' || opcio=='2' || opcio=='3' || opcio == '0')
				valid=true;
			else
				System.out.println("Has d'escollir una opció vàlida.");
		}while(!valid);
		return opcio;
	}

}
