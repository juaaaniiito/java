package ElPenjat;

import java.util.ArrayList;
import java.util.Scanner;

public class Programa {
	
	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		char opcio;
		boolean acabat=false;
		boolean jugador_creat=false;
		String nom_jugador = "";
		ArrayList<String> jugadors = new ArrayList<String>();
		ArrayList<Integer> victories = new ArrayList<Integer>();
		ArrayList<Integer> derrotes = new ArrayList<Integer>();//ArrayList per poder quadrar les victories i derrotes d'un jugador en concret
		do {
			opcio = mostrarMenu();
			switch(opcio) {
			case('1'):
				Ajuda.mostrar_ajuda();
				break;
			case('2'):
				nom_jugador = Jugador.definir_jugador(jugadors, victories, derrotes);
				jugador_creat=true;
				break;
			case('3'):
				if (jugador_creat==true)
					Jugar.jugar_partida(nom_jugador, victories, derrotes, jugadors);
				else
					System.out.println("\nPrimer has de definir el jugador");
				break;
			case('4'):
				if (jugador_creat==true)
					Jugador.veure_jugador(nom_jugador, victories, derrotes, jugadors);
				else 
					System.out.println("\nPrimer has de definir el jugador");
				break;
			case('0'):
				acabat=true;
				System.out.println("Sortint del joc...");
				break;
			default:
				System.out.println("Valor no vàlid");
			}
		} while(!acabat);

	}

	private static char mostrarMenu() {
		char opcio;
		boolean correcte =false;
		do {
			System.out.println("\nPenjat: \n");
			System.out.println("1.-Mostrar ajuda");
			System.out.println("2.-Definir Jugador");
			System.out.println("3.-Jugar partida");
			System.out.println("4.-Veure jugador");
			System.out.println("0.-Sortir\n");
			System.out.print("Escull una opció: ");
			opcio=src.nextLine().charAt(0);
			if (opcio=='1' || opcio=='2' || opcio=='3'|| opcio=='4' || opcio=='0')
				correcte=true;
			if (!correcte)
				System.out.println("Error, has d'introduïr un valor correcte.");
		} while(!correcte);
		
		return opcio;
	}

}
