package ElPenjat;
import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {
	
	static Scanner src = new Scanner(System.in);
	
	public static String definir_jugador(ArrayList<String> jugadors, ArrayList<Integer> victories, ArrayList<Integer> derrotes) {
		
		String nom_jugador ="";
		boolean correcte = false;
		while (!correcte) {
			System.out.println("Vols crear un jugador nou? (Si o No)");
			String resposta = src.next().toLowerCase();
			if (resposta.equals("si")||resposta.equals("no")) {
				if (resposta.equals("si")) {
					System.out.print("\nEscriu el nom del jugador: ");
					nom_jugador = src.next();
					if (jugadors.contains(nom_jugador))
						System.out.println("Aquest nom ja existeix per un jugador, tria un altre.");
					else {
						jugadors.add(nom_jugador);
						victories.add(0);
						derrotes.add(0);
						correcte=true;
					}
				} else {
					System.out.print("\nEscriu el nom del jugador: ");
					nom_jugador = src.next();
					if (!jugadors.contains(nom_jugador)) {
						System.out.println("Aquest jugador no existeix, assegurat d'escriure-ho bé.");
					} else {
						correcte=true;
					}
						
				}
			} else {
				System.out.println("Escriu \"si\" o \"no\"");
			}
		}
		
		return nom_jugador;
	}

	public static void veure_jugador(String nom_jugador, ArrayList<Integer> victories, ArrayList<Integer> derrotes, ArrayList<String> jugadors) {
		System.out.println("\nNom del jugador: " + nom_jugador);
		System.out.print("Victories: ");
		for (int i= 0; i<jugadors.size();i++)
			if (jugadors.get(i).equals(nom_jugador)) {
				System.out.print(victories.get(i));
			}
		System.out.print("\nDerrotes: ");
		for (int i= 0; i<jugadors.size();i++)
			if (jugadors.get(i).equals(nom_jugador)) {
				System.out.print(derrotes.get(i));
			}
		
	}

}