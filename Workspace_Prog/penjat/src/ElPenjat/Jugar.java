package ElPenjat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugar {
	
	static Scanner src = new Scanner(System.in); 

	public static void jugar_partida(String nom_jugador, ArrayList<Integer> victories, ArrayList<Integer> derrotes, ArrayList<String> jugadors) {
		
		String[] llista_paraules= {"taula", "cadira", "pilota", "futbol", "gorra", "telefon", "ordinador", "camisa"};
		Random random = new Random();
		String paraula_secreta= llista_paraules[random.nextInt(llista_paraules.length-1)]; //Prepara la paraula que l'usuari ha d'esbrinar
		ArrayList<Character> paraula_Usuari = preparar_paraula_Usuari(paraula_secreta); //Crida el mètode per preparar la paraula que farem servir amb l'usuari
		ArrayList<Character> lletres_Usades = new ArrayList<Character>();
		final int errors_maxims = 5;
		int errors_usuari =0;
		boolean juguem=true;
		
		while(juguem== true) { //Mentre que no guanyi ni perdi, vés jugant...
			MostrarSituacio(errors_usuari, errors_maxims, paraula_Usuari, lletres_Usades, nom_jugador);
			char lletraActual = ObtenirLletra(lletres_Usades);
			lletres_Usades.add(lletraActual);
			errors_usuari = ComprovarErrors(lletraActual, paraula_Usuari, paraula_secreta, errors_usuari);
			juguem = comprovar_fi(juguem, errors_usuari, errors_maxims,paraula_Usuari, paraula_secreta, nom_jugador, jugadors, victories, derrotes);
			
		}
	}

	private static boolean comprovar_fi(boolean juguem, int errors_usuari, int errors_maxims, 
			ArrayList<Character> paraula_Usuari, String paraula_secreta, String nom_jugador, 
			ArrayList<String> jugadors, ArrayList<Integer> victories, ArrayList<Integer> derrotes) {
		if (errors_maxims<=errors_usuari) {
			System.out.println("T'has equivocat massa vegades, has perdut!!\nLa paraula era: "+ paraula_secreta);
			for (int i= 0; i<jugadors.size();i++)
				if (jugadors.get(i).equals(nom_jugador)) {
					derrotes.set(i, derrotes.get(i)+1);
				}
			return false;
		}
		for (int i = 0; i<paraula_secreta.length();i++) {
			if (paraula_secreta.charAt(i)!=paraula_Usuari.get(i))
				return true;
		}
		System.out.println("\nFELICITATS, HAS GUANYAT!!");
		for (int i= 0; i<jugadors.size();i++)
			if (jugadors.get(i).equals(nom_jugador)) {
				victories.set(i, victories.get(i)+1);
			}
		return false;
	}


	private static int ComprovarErrors(char lletraActual, ArrayList<Character> paraula_Usuari, String paraula_secreta,
			int errors_usuari) {
		boolean trobada=false;
		for (int i = 0; i<paraula_secreta.length();i++) {
			if (paraula_secreta.charAt(i)==lletraActual) {
				paraula_Usuari.set(i, paraula_secreta.charAt(i));
				trobada=true;
			}
		}
		if (!trobada) {
			System.out.println("No es troba aquesta lletra.");
			return errors_usuari+1;
		}
		return errors_usuari;
	}
	
	private static char ObtenirLletra(ArrayList<Character> lletres_Usades) {
		boolean lletra_correcte=false;
		char lletra;
		do {
			System.out.print("\nEscriu una lletra: ");
			lletra = src.nextLine().toLowerCase().charAt(0);
			if (lletres_Usades.contains(lletra)) 
				System.out.print("Ja has fet servir aquesta lletra, escull un altre.");
			else
				lletra_correcte=true;
		} while(!lletra_correcte);
		
		return lletra;
	}


	private static void MostrarSituacio(int errors_usuari, int errors_maxims, ArrayList<Character> paraula_Usuari, ArrayList<Character> lletres_Usades, String nom_jugador) {
		System.out.println("\nJugador actual: " + nom_jugador);
		System.out.println("\nParaula actual:");
		for (int i =0; i<paraula_Usuari.size();i++) {
			System.out.print(paraula_Usuari.get(i)+ " ");
		}
		//int errors_restants = calcular_Errors(errors_usuari, errors_maxims);
		System.out.print("\nErrors restants: ");
		System.out.print(errors_maxims-errors_usuari + "\n");
		System.out.print("Lletres usades: ");
		if (lletres_Usades.size()==0)
			System.out.print("Encara no has escrit cap...\n");
		else
			for (int i = 0; i < lletres_Usades.size(); i++) {
				System.out.print(lletres_Usades.get(i)+" ");
			}
		
	}

	private static ArrayList<Character> preparar_paraula_Usuari(String paraula_secreta) {
		ArrayList<Character> paraula_Usuari = new ArrayList<Character>();
		for(int i = 0; i < paraula_secreta.length(); i++) {
			paraula_Usuari.add('_');
		}
		return paraula_Usuari;
	}

}