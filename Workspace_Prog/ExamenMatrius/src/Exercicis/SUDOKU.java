package Exercicis;

import java.util.Scanner;

public class SUDOKU {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int casos = src.nextInt();
		for (; casos>0; casos--) {
			int[][] tauler = new int[3][3];
			for (int i=0; i<3; i++)
				for (int j=0;j<3;j++)
					tauler[i][j]= src.nextInt();
			int suma_diagonal=0;
			int suma_diagonal2=0;
			Boolean no_premi=false;
			int suma_anterior=0;
			int cont=0;
			for (int i =0; i<3; i++) { //Recorrer cada fila:
				int suma_fila=0;
				for (int j=0; j<3; j++) {
					suma_fila+=tauler[i][j]; //Sumar els valors de cada col·lumna (Suma_fila)
					if (i==j) {
						suma_diagonal+= tauler[i][j]; //Sumem els valors de les diagonals
					}
					if (i+j==j) {
						suma_diagonal2+= tauler[i][j];
					}
				}
				if (cont>0 &&suma_fila!=suma_anterior)
					no_premi=true;
				suma_anterior=suma_fila;
				cont++;
			}
			Boolean no_premi2=false;
			suma_anterior=0;
			cont=0;
			for (int j=0; j<3; j++) { //Recorrer cada col·lumna
				int suma_columna=0;
				for (int i =0; i<3; i++) {
					suma_columna+=tauler[i][j]; //Sumar els valors de cada fila (Suma_columna)
				}
				if (cont>0 &&suma_columna!=suma_anterior)
					no_premi2=true;
				suma_anterior=suma_columna;
				cont++;
			}
			if (!no_premi && !no_premi2) {
				System.out.println("PREMI!");
			} else {
				System.out.println("Sense Premi");
			}
		}
	}
}