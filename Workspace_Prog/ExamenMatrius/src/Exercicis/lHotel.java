package Exercicis;

import java.util.Scanner;

public class lHotel {

	public static void main(String[] args) {
		Scanner src= new Scanner(System.in);
		System.out.println("Indica les plantes de l'hotel: ");
		int plantes= src.nextInt();
		System.out.println("Indica les habitacions per cada planta: ");
		int habitacions= src.nextInt();
		char[][] hotel = new char[plantes][habitacions];
		for (int i =0; i<plantes; i++)
			for (int j =0; j<habitacions; j++)
				hotel[i][j]= '-';
		Boolean tractar = true;
		while (tractar==true) {
			System.out.println("Habitacions actuals: ");
			for (int i =0; i<plantes; i++) {
				for (int j =0; j<habitacions; j++)
					System.out.print(hotel[i][j]+ " ");
				System.out.println();
			}
			System.out.println("Indica el numero d'habitació: ");
			int num_habitacio= src.nextInt();
			int fila= num_habitacio/100;
			int columna= num_habitacio%100;
			if (fila<0|| columna<0 ||fila>=plantes || columna>=habitacions) { //Comprovem els errors d'entrada
				System.out.println("Has introduit una habitació incorrecte.");
			} else {
				if (hotel[fila][columna]=='*') {
					System.out.println("Estat actual: ocupada");
					System.out.println("Dessitja canviar-la d'ocupada a lliure? (S/N)");
					String ocu_lliure= src.next();
					if (ocu_lliure.equals("S"))
						hotel[fila][columna]='-';
				} else {
					System.out.println("Estat actual: lliure");
					System.out.println("Dessitja canviar-la de lliure a ocupada? (S/N)");
					String lliure_ocu= src.next();
					if (lliure_ocu.equals("S"))
						hotel[fila][columna]='*';
				}
			}
			System.out.println("Vol tractar més habitacions? (S/N)");
			String tractar_str = src.next();
			if (tractar_str.equals("N")) {
				tractar=false;
			}
		}
		
		

	}

}
