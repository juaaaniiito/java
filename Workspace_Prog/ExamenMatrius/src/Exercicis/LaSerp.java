package Exercicis;

import java.util.Scanner;

public class LaSerp {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		int N = src.nextInt();
		int M = src.nextInt();
		String[][] serp= new String[N+2][M+2];
		Boolean limits = false;
		int fila_serp = src.nextInt()+1;
		int col_serp = src.nextInt()+1;
		src.nextLine();
		for (int i=0; i<N+2;i++)
			for (int j=0; j<M+2;j++) {
				if (i==0 || i== N+1|| j==0|| j== M+1) { //Dibuixem limits i taulell
					serp[i][j]="X";
				} else {
					serp[i][j]=" ";
				}
			}
		serp[fila_serp][col_serp]= "*";
		String moviments_str = src.nextLine();
		moviments_str=moviments_str.toUpperCase();
		int moviments=0;
		int pos=0;
		while (!limits && pos<moviments_str.length()) {
			switch (moviments_str.charAt(pos)) { //Determinem el moviment
			case('N'):
				fila_serp=fila_serp-1;
				break;
			case('S'):
				fila_serp=fila_serp+1;
				break;
			case('E'):
				col_serp=col_serp+1;
				break;
			case('O'):
				col_serp=col_serp-1;
			}
			moviments++;
			if (serp[fila_serp][col_serp].equals("X")){//Fora dels limits
				limits=true;
			} else {
				serp[fila_serp][col_serp]="*";
			}
			pos++;
		}
		for (int i=1;i<N+1; i++) {
			for (int j=1;j<M+1; j++) {
				System.out.print(serp[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("S'han fet "+ moviments+ " moviments");
		if (limits==true)
			System.out.println("la serp ha sobrepassat els límits del tauler");
	}
}