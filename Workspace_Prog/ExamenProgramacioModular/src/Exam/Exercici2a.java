package Exam;

import java.util.Scanner;

public class Exercici2a {
	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.print("Escriu un nombre enter: ");
		long num = src.nextLong();
		System.out.println("El nombre " + num + " té " + CalcularXifres(num) + " xifres.");
	}

	private static int CalcularXifres(long num) {
		int resultat;
		if (num>-10 && num<0)
			return 1;
		else if (num<10 && num>0)
			return 1;
		resultat = 1 + CalcularXifres(num/10);		
		return resultat;
	}
}