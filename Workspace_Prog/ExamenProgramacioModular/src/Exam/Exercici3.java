package Exam;

import java.util.Scanner;

public class Exercici3 {
	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.print("Escriu una frase: ");
		String[] frase = src.nextLine().split("");
		String[] frase2 = new String[frase.length];
		int pos=frase.length;
		String frase_inv="";
		int pos2=0;
		frase2 = InvertirFrase(frase, frase2, pos, pos2);
		System.out.print("\nLa teva frase al revès: ");
		for (int i = 0; i<frase2.length;i++)
			System.out.print(frase2[i]);
	}

	private static String[] InvertirFrase(String[] frase, String[] frase2, int pos, int pos2) {
		if (pos<0)
			return frase2;
		else {
			frase2[pos2] = frase[pos];
			InvertirFrase(frase, frase2, pos-1,pos2+1);
		}
		return frase2;
	}
}