package Mastermind;

import java.util.Scanner;

public class Programa {
	
	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		boolean JocAcabat = false;
		char opcio;
		boolean generat = false;
		int[] combinacio = new int[4];
		int jugades = 0;
		String ResultatPartida = "";
		int victories=0;
		int derrotes=0;
		int no_finalitzades =0;
		do {
			opcio = MostrarMenu();
			switch (opcio) {
			case('1'):
				combinacio = Jugar.GenComb();
				generat=true;
				jugades=0;
				break;
			case('2'):
				if (!generat) {
					System.out.println("Primer has de Generar la combinació.\n");
				} else {
					ResultatPartida = Jugar.JugarPartida(combinacio);
					if (ResultatPartida.equals("victoria"))
						victories++;
					else if(ResultatPartida.equals("derrota"))
						derrotes++;
					else
						no_finalitzades++;
					generat = false;
				}
				break;
			case('3'):
				Jugar.MostrarAcumulats(victories, derrotes, no_finalitzades);
				break;
			case('0'):
				System.out.println("Gracies per jugar.");
				JocAcabat=true;
				break;
			}
			/*
			int pausa = src.nextInt();
			
			for (int i = 0; i<4; i++)
			System.out.print(combinacio[i]);*/
		}while(!JocAcabat);
	}

	private static char MostrarMenu() {
		char opcio;
		boolean OpcioCorrecte = false;
		System.out.println("--MASTERMIND--\n");
		System.out.println("1. Generar combinació.");
		System.out.println("2. Jugar.");
		System.out.println("3. Resultats Acumulats.");
		System.out.println("0. Sortir");
		System.out.print("\nEscull una opció: ");
		do {
			opcio = src.next().charAt(0);
			if (opcio == '1' || opcio == '2' || opcio == '3' || opcio == '0')
				OpcioCorrecte=true;
			else
				System.out.println("Has d'escollir una opció vàlida.");
		} while(!OpcioCorrecte);
		return opcio;
	}
}
