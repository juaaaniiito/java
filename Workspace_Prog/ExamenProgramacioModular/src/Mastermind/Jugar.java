package Mastermind;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugar {
	
	static Random rd = new Random();
	static Scanner src = new Scanner(System.in);
	
	public static int[] GenComb() {
		int[] combinacio = new int[4];
		ArrayList<Integer> usats = new ArrayList<Integer>();
		int i=0;
		while(i<4) {
			int num= rd.nextInt(0, 10);
			if (!usats.contains(num)) {
				combinacio[i]=num;
				usats.add(num);
				i++;
			}
		}
		return combinacio;
	}

	public static String JugarPartida(int[] combinacio) {
		String resultat="";
		char resultat2=' ';
		int tirades_max = 15;
		int num_tirades = 0;
		
		do {
			int[] tirada = new int[4];
			if(num_tirades>0)
				resultat2 = MostrarMenuPartida();
			tirada = ComprovarTirada();
			resultat = ComprovarResultat(combinacio, tirada);
			if(num_tirades<tirades_max)
				resultat.equals("derrota");
			num_tirades++;
		}while((!resultat.equals("victoria")||!resultat.equals("derrota")) && num_tirades<tirades_max && resultat2=='0');
		return resultat;
	}

	
	
	
	private static char MostrarMenuPartida() {
		char opcio;
		boolean OpcioCorrecte = false;
		System.out.println("1. Fer una tirada");
		System.out.println("0. Sortir");
		System.out.print("\nEscull una opció: ");
		do {
			opcio = src.next().charAt(0);
			if (opcio == '1' || opcio == '0')
				OpcioCorrecte=true;
			else
				System.out.println("Has d'escollir una opció vàlida.");
		} while(!OpcioCorrecte);
		return opcio;
	}

	private static String ComprovarResultat(int[] combinacio, int[] tirada) {
		int numeros_encertats=0;
		int numeros_plens=0;
		String resultat="";
		for (int i =0; i<combinacio.length;i++) {
			if (combinacio[i]==tirada[i])
				numeros_plens++;
			for (int j = 0; j<combinacio.length;i++) {
				if (combinacio[i]==tirada[j]) {
					numeros_encertats++;
				}
			}
		}
		if(numeros_plens==4)
			resultat="victoria";
		return resultat;
	}

	private static int[] ComprovarTirada() {
		boolean bona = false;
		System.out.println("Escriu una combinació de 4 números entre 0 i 9\nsense repeticions: ");
		String[] frase = src.next().split("");
		int i =0;
		int[] numeros = new int[4];
		
		while(i<4) {
			numeros[i]=Integer.valueOf(frase[i]);
			i++;
		}
		/*ArrayList<Integer> usats2 = new ArrayList<Integer>();
		int pos=0;
		
		do {
			try {
				System.out.print("Escriu una combinació de 4 números entre 0 i 9\nsense repeticions: ");
				String[] frase = src.nextLine().split("");
				numeros[pos]= Integer.valueOf(frase[pos]);
				if (!usats2.contains(numeros[pos])) {
					usats2.add(numeros[pos]);
					pos++;
				} else
					System.out.println("No poden repetir-se.");
			}
			catch(Exception e){
				System.out.println("Has d'escriure un nombre enter.");
			}
			if (numeros[3]>-1 && numeros[3]<10)
				bona=true;
		} while(!bona);*/
		
		return numeros;
	}

	public static void MostrarAcumulats(int victories, int derrotes, int no_finalitzades) {
		System.out.println("Partides guanyades: "+ victories);
		System.out.println("Partides perdudes: "+ derrotes);
		System.out.println("Partides no_finalitzades: "+ no_finalitzades);
		
	}

}
