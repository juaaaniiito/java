package ExamemPOO;

public class Rosa extends Regal implements Flor{
	private ColorRosa color = ColorRosa.VERMELL;
	private QualitatRosa qualitat = QualitatRosa.MITJANA;
	
	//GETTERS

	public ColorRosa getColor() {
		return color;
	}
	public QualitatRosa getQualitat() {
		return qualitat;
	}
	
	
	//CONSTRUCTORS
	public Rosa() {
		super();
		preuFinal();
	}
	
	public Rosa(String color, String qualitat) {
		super();
		QualitatRosa quaAssig = null;
		quaAssig = comprovarQualitat(qualitat);
		ColorRosa colAssig = null;
		colAssig = comprovarColor(color);
		if (quaAssig != null)
			this.qualitat = quaAssig;
		if (colAssig != null)
			this.color = colAssig;
		preuFinal();
	}
	
	public Rosa(int estoc, float preu, String color, String qualitat) {
		super(preu, estoc);
		QualitatRosa quaAssig = null;
		quaAssig = comprovarQualitat(qualitat);
		ColorRosa colAssig = null;
		colAssig = comprovarColor(color);
		if (quaAssig != null)
			this.qualitat = quaAssig;
		if (colAssig != null)
			this.color = colAssig;
		preuFinal();
	}
	
	//MÈTODES DE L'OBJECTE
	
	private QualitatRosa comprovarQualitat(String c) {
		if(c.toUpperCase().equals("ALTA"))
			return QualitatRosa.ALTA;
		else if (c.toUpperCase().equals("MITJANA"))
			return QualitatRosa.MITJANA;
		else if (c.toUpperCase().equals("BAIXA"))
			return QualitatRosa.BAIXA;
		else
			return null;
	}
	
	private ColorRosa comprovarColor(String color) {
		if (color.toUpperCase().equals("BLAU"))
			return ColorRosa.BLAU;
		else if (color.toUpperCase().equals("BLANC"))
			return ColorRosa.BLANC;
		else if (color.toUpperCase().equals("VERMELL"))
			return ColorRosa.VERMELL;
		else
			return null;
	}
	
	@Override
	public void preuFinal() {
		if (this.qualitat == QualitatRosa.ALTA)
			this.setPreuVenda(this.getPreuVenda()+5);
		if (this.qualitat == QualitatRosa.BAIXA)
			this.setPreuVenda(this.getPreuVenda()-5);
		if (this.color == ColorRosa.BLANC)
			this.setPreuVenda(this.getPreuVenda()+2);
		if (this.color == ColorRosa.BLAU)
			this.setPreuVenda(this.getPreuVenda()+5);
	}
	
	@Override
	public String toString() {
		String res = "";
		res = super.toString();
		return res + "\nDades de la rosa:\n    Color: " + color + "    Qualitat: " + qualitat;
	}
	@Override
	public void novaQualitat(int dies) {
		if (dies>2) {
			if (this.qualitat == QualitatRosa.BAIXA) {
				this.preuVenda = 0;
			} else if (this.qualitat == QualitatRosa.MITJANA) {
				this.qualitat = QualitatRosa.BAIXA;
				this.preuVenda = this.getPreuVenda()-2;
			} else {
				this.qualitat = QualitatRosa.MITJANA;
				this.preuVenda = this.getPreuVenda()-5;
			}
		}
	}
	
	
	
}
