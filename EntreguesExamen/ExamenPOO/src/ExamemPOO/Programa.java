package ExamemPOO;

import java.util.Random;
import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		Random rd = new Random();
		Magatzem mg1 = new Magatzem();
		mg1.omplirMagatzem(20);
		//Creo un regal de cada tipus per pasar-los per paràmetre.
		Regal l1 = new Llibre();
		Regal r1 = new Rosa();
		System.out.println("--------------------");
		System.out.println("MOSTREM ELS LLIBRES:");
		System.out.println("--------------------");
		mg1.regalsEquivalents(l1);
		System.out.println("Suma total del preu de tots els llibres: " + mg1.sumaRegals(l1));
		System.out.println("Mitjana del preu de tots els llibres: " + mg1.mitjanaRegals(l1));
		
		System.out.println("--------------------");
		System.out.println("-MOSTREM LES ROSES:-");
		System.out.println("--------------------");
		mg1.regalsEquivalents(r1);
		System.out.println("Suma total del preu de totes les roses: " + mg1.sumaRegals(r1));
		System.out.println("Mitjana del preu de totes les roses: " + mg1.mitjanaRegals(r1));
		
		//VENDA DE REGALS
		int posRegal = 0;
		while (posRegal>=0) {
			posRegal = src.nextInt();
			mg1.vendaRegal(posRegal);
		}
		System.out.println("--------------------");
		System.out.println("--MAGATZEM(ROSES)---");
		System.out.println("--------------------");
		mg1.visualitzarMagatmem("ROSA");
		System.out.println("--------------------");
		System.out.println("-MAGATZEM(LLIBRES)--");
		System.out.println("--------------------");
		mg1.visualitzarMagatmem("LLIBRE");
		
		int randPosRegal = rd.nextInt(mg1.getNumActual());
		for (int i= 0; i<mg1.getNumActual(); i++) {
			if (mg1.obtenirRegal(randPosRegal).equals(mg1.obtenirRegal(i))) {
				System.out.println(mg1.obtenirRegal(i).toString());
			}
		}
		
		System.out.println("----------------------");
		System.out.println("-AMPLIACIÓ, APARTAT G-");
		System.out.println("----------------------");
		
		for (int i = 0; i<mg1.getNumActual(); i++) {
			if (mg1.obtenirRegal(i) instanceof Rosa) {
				int randDies = rd.nextInt(6);
				((Rosa)mg1.obtenirRegal(i)).novaQualitat(randDies);
			}
			if (mg1.obtenirRegal(i) instanceof Llibre) {
				((Llibre)mg1.obtenirRegal(i)).actualitzarPreu();
			}
		}
		
		System.out.println("----------------------");
		System.out.println("--MAGATZEM AL ACABAR--");
		System.out.println("----------------------");
		System.out.println("-------ROSES---------");
		mg1.visualitzarMagatmem("ROSA");
		System.out.println("------LLIBRES--------");
		mg1.visualitzarMagatmem("LLIBRE");
		
	}

}
