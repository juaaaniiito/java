package ExamemPOO;

public interface GenereLiterari {
	
	public default float aplicarPercentatge(String genere) {
		float Preu = 0;
		if (genere.equals("novel·la")) {
			Preu = (float) 0.10;
		} else if (genere.equals("infantil")) {
			Preu = (float) -0.10;
		} else if (genere.equals("poesia")) {
			Preu = (float) -0.20;
		}
		return Preu;
	}
	
	public abstract void actualitzarPreu();
	
}
