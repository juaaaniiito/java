package capitol1;

import java.util.Scanner;

class Programa {
	
	public static void main(String[] args) {
		Scanner src = new Scanner(System.in);
		System.out.println("Introdueix un numero entre l'1 i el 20.");
		int numCicles = src.nextInt();
		Granja g1 = new Granja();
		g1.generarGranja(10);
		System.out.println("-----------------------------");
		System.out.println("-------GRANJA INICIAL--------");
		System.out.println("-----------------------------");
		g1.visualitzar();
		for (int i = 0; i<numCicles; i++) {
			int total = g1.getNumGossos();
			for (int j = 0; j<total-1; j++) {
				Gos NouGos = null;
				NouGos = g1.obtenirGos(j).aparellar(g1.obtenirGos(j+1));
				if (NouGos != null) {
					g1.afegir(NouGos);
					System.out.println("Ha nascut un nou GOS!!"); // Anunci del nou naixement
					g1.obtenirGos(j).incEdat(); // Incrementa l'edat dels gossos
					g1.obtenirGos(j+1).incEdat();;
					j++; // Començarà per la posició i+2
				} else {
					NouGos = new Gos(); //Fem servir un constructor en un gos temporal per poder clonar el de la posició 'j' a 'j+2'.
					NouGos.clonar(g1.obtenirGos(j)); // Tenim el gos en un gos temporal
					g1.obtenirGos(j).clonar(g1.obtenirGos(j+1)); //Sobreescrivim el gos de la posició 'j+1' a la posició 'j'
					g1.obtenirGos(j+1).clonar(NouGos);
				}
			}
			System.out.println("-----------------------------");
			System.out.println("------HA PASSAT UN ANY-------");
			System.out.println("-----------------------------");
			g1.visualitzar();
		}
		
		
		/* MODIFICACIÓ DE L'APARTAT 1.D
		Granja granja1 = new Granja(); // Generació de la granja
		granja1.generarGranja(5); // Generació de gossos la granja
		Granja granja2 = new Granja();
		granja2.generarGranja(5);
		Granja granja3 = new Granja();
		// AFEGIM ELS POSSIBLES GOSSOS A LA TERCERA GRANJA
		for (int ajuda = 0; ajuda<5; ajuda++) {
			Gos gosPerAfegir = null;
			gosPerAfegir = granja1.obtenirGos(ajuda).aparellar(granja2.obtenirGos(ajuda));
			if (gosPerAfegir != null) {
				granja3.afegir(gosPerAfegir);
			}
			
		}
		//VISUALITZEM LES GRANJES
		System.out.println("MEMBRES DE LA GRANJA 1");
		granja1.visualitzar();
		System.out.println("\nMEMBRES DE LA GRANJA 2");
		granja2.visualitzar();
		System.out.println("\nMEMBRES DE LA GRANJA 3");
		granja3.visualitzar();
		*/
		/* MODIFICACIÓ DEL PROGRAMA DE L'APARTAT 1.C
		Granja GranjaDeJuanjo = new Granja();
		System.out.print("Introdueix el nombre de gossos que vols ficar a la nova granja: ");
		int numGossos = src.nextInt();
		GranjaDeJuanjo.generarGranja(numGossos);
		char continuar = 'n';
		while (continuar!='2') {
			GranjaDeJuanjo.visualitzar();
			System.out.print("Escriu la posició del gos que vols tractar: ");
			int posicio = src.nextInt()-1;
			src.nextLine();
			GranjaDeJuanjo.obtenirGos(posicio);
			System.out.println("Escull quin atribut vols modificar: \n1- Estat. \n2- Edat.");
			char opcio = src.next().charAt(0);
			if (opcio == '1') {
				System.out.println("Per canviar l'estat del gos, escull el nou estat: \n1- Viu \n2- Mort ");
				char opcioEstat = src.next().charAt(0);
				if (opcioEstat=='1')
					GranjaDeJuanjo.obtenirGos(posicio).setEstat(Gos.GosEstat.VIU);
				else
					GranjaDeJuanjo.obtenirGos(posicio).setEstat(Gos.GosEstat.MORT);
			} else if (opcio == '2') {
				//CAMBIAR EL VALOR DE L'EDAT DEL GOS I EL SEU ESTAT
				System.out.println("Introdueix l'edat del nou gos");
				int EdatNova = src.nextInt();
				GranjaDeJuanjo.obtenirGos(posicio).setEdat(EdatNova);
			}
			System.out.println("Escull una opció: \n1- Continuar tractant gossos.\n2- Sortir.");
			continuar = src.next().charAt(0);
		}
		GranjaDeJuanjo.visualitzar();
		*/
		
		/* CREACIÓ DE GOSSOS ALS APARTATS 1.A I 1.B
		Gos pedro = new Gos(3, "pedro", 1, 'M');
		pedro.borda();
		pedro.visualitzar();
		//Còpia d'un gos
		Gos copiaPedro = new Gos(pedro);
		copiaPedro.borda();
		copiaPedro.visualitzar();
		//Només rep el nom del gos, la resta de valors es posen per defecte
		Gos benito = new Gos("Benito");
		benito.borda();
		benito.visualitzar();
		//Gos creat amb valors per defecte
		Gos simplin = new Gos();
		simplin.borda();
		simplin.visualitzar();
		simplin.clonar(benito);
		simplin.visualitzar();
		
		if (benito.getRaça() != null) System.out.println("Raça del gos Benito: " + benito.getRaça().getNomRaça());
		
		System.out.println("\nHas creat un total de " + Gos.quantitatGossos() + " gossos.\n");
		
		Raça r1 = new Raça ("Pitbull", Gos.GosMida.MITJA); 
		
		benito.setRaça(r1);
		benito.visualitzar();
		*/
		/* Apartat 1.A.2
		System.out.println("Edat: " + pedro.getEdat());
		System.out.println("Nom: " + pedro.getNom());
		System.out.println("Fills: " + pedro.getFills());
		System.out.println("Sexe: " + pedro.getSexe());
		pedro.setEdat(src.nextInt());
		pedro.setFills(src.nextInt());
		src.nextLine();
		pedro.setNom(src.nextLine());
		pedro.setSexe(src.nextLine().charAt(0));		
		*/
		/* Apartat 1.A.1
		System.out.println("Edat: " + pedro.edat + "\n"
				+ "Nom: " + pedro.nom + "\nFills: " + 
				pedro.fills + "\nSexe: " + pedro.sexe);
		*/
		src.close();
	}
}
