package capitol1;
import java.util.Random;

public class Gos {

	private int edat = 4;
	private String nom = "SenseNom";
	private int fills = 0;
	private char sexe = 'M';
	private static int qntGossos =0;
	private Raça raça;
	private GosEstat estat = GosEstat.VIU;
	
	public void borda() {
		System.out.println("guau guau");
	}
	//CONSTRUCTORS:
	/*Constructor que rep la raça com paràmetre*/
	public Gos(Raça raça) {
		this();
		this.raça=raça;
		qntGossos++;
	}
	
	/*Constructor que rep tots els valors per paràmetre*/
	public Gos(int edat, String nom, int fills, char sexe) {
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
		qntGossos++;
	}
	
	/*Constructor que còpia els valors d'un altre gos.*/
	public Gos(Gos copia) {
		edat = copia.edat;
		nom = copia.nom;
		fills = copia.fills;
		sexe = copia.sexe;
		qntGossos++;
	}
	
	/*Constructor que només rep el nom del gos i 
	 * la resta de paràmetres els assigna per defecte*/
	public Gos(String nomGos) {
		this.nom = nomGos;
		qntGossos++;
	}
	
	/*Constructor sense paràmetres, per defecte.*/
	public Gos() {
		qntGossos++;
	}

	/* VISUALITZAR L'OBJECTE
	 * Permet visualitzar el toString*/
	public void visualitzar() {
		System.out.println(this.toString());
		System.out.println("/////////////////");
	}
	
	@Override
	public String toString() {
		if (this.raça == null)
			return "Dades del gos:\nedat: " + edat + "\nnom: " + nom + "\nfills: " + fills +
					"\nsexe: " + sexe + "\nraça: No té raça"  + "\nestat: " + estat + "\n";
		return "Dades del gos:\nedat: " + edat + "\nnom: " + nom + "\nfills: " + fills +
				"\nsexe: " + sexe + "\nraça: " + raça + "\nestat: " + estat + "\n";
	}
	//MÈTODES DE L'OBJECTE:
	
	//Aparellament dels gossos
	public Gos aparellar(Gos g) {
		//Gos nouGos = new Gos(); CREACIO DEL NOU GOS
		Gos nouGos = null;
		if (valid(this, g) == true) {
			nouGos = new Gos(); //Dilema de si pasar el nou gos o pasar los anteriores
			nouGos = parametresGossos(this, g, nouGos);
		}
		return nouGos;
	}
	
	
	
	private Gos parametresGossos(Gos gos, Gos g, Gos nouGos) { //Métode per assignar els parametres dels gossos.
		//ASSIGNACIÓ DEL SEXE
		Random rd = new Random();
		char[] sexe = {'M', 'F'};
		nouGos.setSexe(sexe[rd.nextInt(2)]);
		
		//ASSIGNACIÓ DE L'EDAT
		nouGos.setEdat(0);
		
		//ASSIGNACIÓ DEL NOM
		if (nouGos.getSexe()== 'M') {
			if (gos.getSexe()== 'M')
				nouGos.setNom("Fill de " + gos.getNom());
			else
				nouGos.setNom("Fill de " + g.getNom());
		} else {
			if (gos.getSexe()=='F')
				nouGos.setNom("Filla de " + gos.getNom());
			else
				nouGos.setNom("Filla de " + g.getNom());
		}
		
		//ASSIGNACIÓ DE RACES
		if (gos.raça != null && g.raça != null) {
			if (gos.sexe=='M' && gos.raça.isDominant() && !g.raça.isDominant()) {
				nouGos.setRaça(gos.getRaça());
			} else {
				nouGos.setRaça(g.getRaça());
			}
			if (g.sexe == 'M' && g.raça.isDominant() && !gos.raça.isDominant()) {
				nouGos.setRaça(g.getRaça());
			} else {
				nouGos.setRaça(gos.getRaça());
			}
		} else if (gos.raça != null && g.raça == null) {
			nouGos.setRaça(gos.getRaça());
		} else if (gos.raça == null && g.raça != null) {
			nouGos.setRaça(g.getRaça());
		}//Si cap dels dos no té raça, el fill tampoc en tindrà, per tant, no és necessari afegir res(Valor per defecte).
		
		//ACTUALITZACIÓ DELS NUMERO DE FILLS DEL PARE I DE LA MARE
		gos.fills ++;
		g.fills ++;
		
		return nouGos;
	}
	private boolean valid(Gos gos, Gos g) { //Si no funciona, canviar 'this' per 'gos'
		if (this.edat>=2 && this.edat<=10 && g.edat>=2 && g.edat<=10) { //Comprovació de les edats
			if (this.estat == GosEstat.VIU && g.estat == GosEstat.VIU) { //Comprovació de que els gossos estiguin vius
				if (sexe == 'M' && g.sexe == 'F' || sexe == 'F' && g.sexe == 'M') { //Comprovació dels sexes i races dels gossos
					if (this.sexe == 'F' && this.fills<=3 ) { // Comprovació dels fills
						if (this.raça == null || g.raça == null)
							return true;
						else if (this.raça.getMida() == GosMida.GRAN)
							return true;
						else if (this.raça.getMida() == GosMida.MITJA)
							if (g.raça.getMida() == GosMida.MITJA || g.raça.getMida() == GosMida.PETIT)
								return true;
						else if (this.raça.getMida() == GosMida.PETIT)
							if (g.raça.getMida() == Gos.GosMida.PETIT)
								return true;
					} else if (g.sexe == 'F' && g.fills<=3) {
						if (g.raça == null || this.raça == null)
							return true;
						else if (g.raça.getMida() == Gos.GosMida.GRAN)
							return true;
						else if (g.raça.getMida() == Gos.GosMida.MITJA)
							if (this.raça.getMida() == GosMida.MITJA || this.raça.getMida() == GosMida.PETIT)
								return true;
						else if (g.raça.getMida() == Gos.GosMida.PETIT)
							if (this.raça.getMida() == GosMida.PETIT)
								return true;
					}
				}
			}
		}
		return false;
	}
	//Clonació dels gossos
	public void clonar(Gos ungos) {
		this.edat = ungos.edat;
		this.nom = ungos.nom;
		this.fills = ungos.fills;
		this.sexe = ungos.sexe;
		this.raça = ungos.raça;
		this.estat = ungos.estat;
	}
	
	//Mostra quants gossos s'han creat.
	public static int quantitatGossos() {
		return qntGossos;
	}
	
	//Enumeracions de les mides i estats dels gossos
	public enum GosMida {
		GRAN, MITJA, PETIT
	}
	
	public enum GosEstat{
		VIU, MORT
	}
	
	
	//SETTERS I GETTERS
	
	public GosEstat getEstat() {
		return estat;
	}

	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}

	public Raça getRaça() {
		return raça;
	}

	public void setRaça(Raça raça) {
		this.raça = raça;
	}

	public int getEdat() {
		return edat;
	}
	
	public void incEdat() {
		if (getEstat()==GosEstat.VIU) {
			edat++;
			actualitzarEstat();
		}
	}

	private void actualitzarEstat() {
		if (getRaça()!=null) {
			if (edat >= this.getRaça().getTempsVida())
				estat = GosEstat.MORT;
		}
		else 
			if (edat >10)
				estat = GosEstat.MORT;
	}

	
	public void setEdat(int edat) {
		if (edat>=0) {
			this.edat = edat;
			if (raça==null) {
				if (this.edat>=10)
					this.estat = GosEstat.MORT;
			}else {
				if (edat>raça.getTempsVida())
					this.estat = GosEstat.MORT;
			}
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		if (fills>=0)
			this.fills = fills;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		if (sexe == 'M' || sexe == 'F')
			this.sexe = sexe;
	}

	
}

	