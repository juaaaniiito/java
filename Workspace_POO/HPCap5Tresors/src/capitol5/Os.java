package capitol5;

public class Os extends Tresor implements Comestible, Intercanviable{
	private boolean superOs;
	
	//SETTER I GETTER
	public boolean isSuperOs() {
		return superOs;
	}
	public void setSuperOs(boolean superOs) {
		this.superOs = superOs;
	}

	//MÈTODE toString
	@Override
	public String toString() {
		String frase = "\n";
		frase += super.toString();
		return frase + "\nsuperOs: " + superOs;
	}
	
	// CONSTRUCTORS DE L'OBJECTE 'OS'
	public Os() {
		super(false, 4, 4, false);
		this.superOs = false;
	}
	
	public Os(boolean superOs) {
		if (superOs == true) {
			this.valor = 8;
			this.prioritari = true;
			this.duracio = 6;
			this.superOs = true;
		} else
			this.superOs = false;
	}
	
	public Os(boolean prioritari, int valor, int duracio, boolean superOs) {
		super(prioritari, valor, duracio);
		this.superOs = superOs;
	}
	
	// MÈTODES DE L'OBJECTE PER SOBREESCRIURE A 'Tresor'
	
	@Override
	public void enterrar() {
		if (!enterrat) {
			super.enterrar();
			if (!superOs) {
				if (this.getValor()>=1)
					this.setValor(this.getValor()-1);
				if (this.getDuracio()>=1)
					this.setDuracio(this.getDuracio()-1);
			}
		}
	}
	
	@Override
	public void desenterrar() {
		if (enterrat) {
			super.desenterrar();
			if (this.getValor()>=1)
				this.setValor(this.getValor()-1);
		}
	}
	
	@Override
	public boolean interCanviar(Tresor t) {
		if (t instanceof Os)
			if (((Os) t).isSuperOs()) return true;
		return false;
	}
}
