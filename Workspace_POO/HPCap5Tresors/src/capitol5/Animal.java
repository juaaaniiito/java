package capitol5;
import java.util.Random;

/**
 * Aquesta classe està destinada a la creació de l'objecte Animal, declarant els seus constructors i mètodes corresponents.
 * @author Juan José Rodríguez
 *
 */
public abstract class Animal {
	protected int edat;
	protected String nom;
	protected int fills;
	protected AnimalSexe sexe;
	protected AnimalEstat estat;
	static int numAnimals;
	
	//CONSTRUCTORS
	public Animal() {
		Random rd = new Random();
		edat = 0;
		nom = "sense nom";
		fills = 0;
		int numSexe = rd.nextInt(2);
		if (numSexe == 0) //Decidir sexe per defecte
			sexe = AnimalSexe.M;
		else
			sexe = AnimalSexe.F;
		estat = AnimalEstat.VIU;
		numAnimals++;
	}
	
	public Animal(String nom2, int edat2, char sexe2) {
		this();
		this.nom = nom2;
		this.edat = edat2;
		if (sexe2 == 'M')
			this.sexe = AnimalSexe.M;
		else if (sexe2 == 'F')
			this.sexe = AnimalSexe.F;
	}

	public Animal(int edat, String nom, int fills, AnimalSexe sexe, AnimalEstat estat) {
		this();
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
		this.estat = estat;
	}

	
	public Animal(String n) {
		this();
		this.nom = n;
	}

	
	
	//GETTERS
	public int getEdat() {
		return edat;
	}
	public String getNom() {
		return nom;
	}
	public int getFills() {
		return fills;
	}
	public AnimalSexe getSexe() {
		return sexe;
	}
	public AnimalEstat getEstat() {
		return estat;
	}
	public static int getNumAnimals() {
		return numAnimals;
	}
	
	
	//SETTERS
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
		public void setFills(int fills) {
		this.fills = fills;
	}
	public void setSexe(AnimalSexe sexe) {
		this.sexe = sexe;
	}
	public void setEstat(AnimalEstat estat) {
		this.estat = estat;
	}
		
	
	//MÉTODES DE L'OBJECTE
	/**
	 * Mètode que simula una salutació d'un animal.
	 */
	public abstract void so();
	
	/**
	 * Mètode que aparella dos animals i en crea un de nou.
	 */
	public abstract Animal aparellar(Animal nuvi);
	/**
	 * Mètode que comprova la compatibilitat dels dos animals.
	 * @param animal Primer animal que es vol aparellar. 
	 * @param nuvi Segon animal que es vol aparellar.
	 * @return Indica si la relació ha resultat satisfactòria o no.
	 */
	protected boolean comprovarConnexio(Animal animal, Animal nuvi) {
		if (animal.getEstat() == AnimalEstat.VIU && nuvi.getEstat() == AnimalEstat.VIU) {
			if (animal.getSexe() == AnimalSexe.M)
				if (nuvi.getSexe() == AnimalSexe.F)
					return true;
			else
				if (animal.getSexe() == AnimalSexe.M)
					return true;
		}
		return false;
	}
	
	/**
	 * Mètode que clona un animal amb el que es passa per paràmetre.
	 * @param model Animal del qual es copiaran les dades.
	 */
	public void clonar(Animal model) {
		this.edat = model.edat;
		this.nom = model.nom;
		this.fills = model.fills;
		this.sexe = model.sexe;
		this.estat = model.estat;
	}
	
	/**
	 * Mètode que augmenta en 1 l'edat del animal en cas de que estigui viu.
	 */
	public void incEdat() {
		if (this.estat == AnimalEstat.VIU)
			this.edat++;
	}
	
	/**
	 * Mètode que permet visualitzar l'objecte 'ANIMAL'.
	 */
	public void visualitzar() {
		System.out.println("DADES DE L'ANIMAL:" + this.toString());
	}
	
	/**
	 * Mètode que converteix les dades de l'objecte animal a un String.
	 */
	@Override
	public String toString() {
		return "Edat: " + edat + "\nNom: " + nom + "\nFills: " + fills + "\nSexe: " + sexe + "\nEstat: " + estat;
	}


	// CLASSES "ENUMS" DEL SEXE I L'ESTAT
	/**
	 * Enumeració que indica el sexe de l'animal
	 */
	
	/**
	 * Enumeració que indica l'estat actual de l'animal
	 */
	
	
}
