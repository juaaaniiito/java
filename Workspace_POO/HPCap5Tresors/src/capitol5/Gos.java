package capitol5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Gos extends Animal {
	private List<Tresor> llistaTresors = new ArrayList<Tresor>();
	private Tresor tresorActiu;
	private int power = 0;
	private Raça raça;
	
	//CONSTRUCTORS:
	/*Constructor sense paràmetres, per defecte.*/
	public Gos() {
		super();
		raça = null;
	}
	
	public Gos(int edat, String nom, int numFills, AnimalSexe sexe, AnimalEstat estat, Raça raça) {
		super(edat, nom, numFills, sexe, estat);
		this.raça = raça;

	}
	
	/*Constructor que rep tots els valors per paràmetre*/
	public Gos(String nom, int edat, Raça raça, char sexe) {
		super(nom, edat, sexe);
		this.raça = raça;
	}
	
	
	
	public Gos (String n) {
		super(n);
		this.raça=null;
	}
	
	
	/*Constructor que còpia els valors d'un altre gos.*/
	public Gos(Gos g) {
		this();
		clonar(g);
	}
	
	//MÈTODES INDEPENDENTS DELS GOSSOS
	public void afegirTresor(Tresor t) {
		if (this.tresorActiu != null) {
			this.llistaTresors.add(t);
		}
		this.tresorActiu = t;
	}
	
	public Tresor rescatarTresor() {
		if (!llistaTresors.isEmpty()) 
			tresorActiu = llistaTresors.remove(0);
		return tresorActiu;
	}
	
	public int valorMenjar() {
		int sumaValors = 0;
		for (int i = 0; i < llistaTresors.size(); i++) {
			if (llistaTresors.get(i) instanceof Comestible)
				sumaValors = sumaValors+llistaTresors.get(i).getValor();
		}
		return sumaValors;
	}
	
	public void renovar() {
		Random rd = new Random();
		if (this.tresorActiu != null && this.llistaTresors.size()!=0) {
			int randLista = rd.nextInt(this.llistaTresors.size());
			this.llistaTresors.add(tresorActiu);
			this.tresorActiu = this.llistaTresors.remove(randLista);
		}
	}
	
	public int powerTresors() {
		int sumaPoder = 0;
		for (int i = 0; i<this.llistaTresors.size(); i++) {
			sumaPoder += this.llistaTresors.get(i).getValor();
		}
		return sumaPoder;
	}
	
	public int gosPower() {
		return this.tresorActiu.getValor() + this.getPower() + powerTresors();
	}

	//MÈTODES DE L'OBJECTE PER SOBREESCRIURE 'Animal.java'
	@Override
	public void visualitzar() {
		System.out.println("Dades del gos:");
		System.out.println(this.toString());
		System.out.println("/////////////////");
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		if (this.raça != null)
			cadena += "\nRaça: " + raça.getNomRaça();
		else
			cadena += "\nRaça: Desconeguda";
		for (int i = 0; i<llistaTresors.size(); i++) { // Alomillor s'ha de canviar la manera de mostrar-lo per pantalla.
			cadena += llistaTresors.get(i).toString();
		}
		return cadena;
	}
	
	@Override
	public void clonar(Animal a) {
		if (a instanceof Gos) {
			super.clonar(a);
			this.raça = ((Gos)a).raça;
		}
	}
	
	@Override
	public void so() {
		System.out.println("guau guau");
	}
	
	@Override
	public void incEdat() {
		super.incEdat();
		if (this.raça == null && this.getEdat()>=10) {
			this.setEstat(AnimalEstat.MORT);
		} else if (this.raça != null && this.getEdat() > this.raça.getTempsVida()) {
			this.setEstat(AnimalEstat.MORT);
		}
	}
	
	
	@Override
	public Animal aparellar(Animal a) {
		Animal fill = null;
		
		if (!(a instanceof Gos)) return null;
		
		if (this.comprovarConnexio(this, a)) {
			fill = new Gos();
			this.setFills(this.getFills()+1);
			a.setFills(a.getFills()+1);
			return new Gos(fill.edat, fill.nom, fill.fills, fill.sexe, fill.estat, this.raça);
		} else
			return null;
	}
	
	// GETTERS I SETTERS
	
	public Raça getRaça() {
		return raça;
	}
	
	public List<Tresor> getLlistaTresors() {
		return llistaTresors;
	}

	public Tresor getTresorActiu() {
		return tresorActiu;
	}

	public int getPower() {
		return power;
	}

	public void setLlistaTresors(List<Tresor> llistaTresors) {
		this.llistaTresors = llistaTresors;
	}

	public void setTresorActiu(Tresor tresorActiu) {
		this.tresorActiu = tresorActiu;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public void setRaça(Raça raça) {
		this.raça = raça;
	}
}

	