package capitol5;

import java.util.Random;

public class Programa {

	public static void main(String[] args) {
		Random rd = new Random();
		//CAPITOL 5B
		
		Raça r1 = new Raça("Bulldog", GosMida.PETIT);
		Raça r2 = new Raça("Caniche", GosMida.MITJA);
		
		Gos g1 = new Gos(20, "Kenu", 0, AnimalSexe.M, AnimalEstat.VIU, r1);
		Gos g2 = new Gos(18, "Maresme", 0, AnimalSexe.M, AnimalEstat.VIU, r2);
		
		Tresor IronMan = new Joguina(estatJoguina.NOU);
		Tresor BlackPanther = new Joguina(estatJoguina.NORMAL);
		Tresor Bobux = new Llaminadura();
		Tresor VaL = new Os();
		
		g1.afegirTresor(IronMan);
		//g1.afegirTresor(Bobux);
		g1.rescatarTresor();
		
		//Assignem un valor a la joguina per a que sigui intercanviable.
		BlackPanther.setValor(9);
		
		g2.afegirTresor(BlackPanther);
		//g2.afegirTresor(VaL);
		g2.rescatarTresor();
		
		System.out.println("Dades dels tresors dels gossos abans de fer el canvi: \n");
		System.out.println(g1.getTresorActiu().toString()); 
		System.out.println(g2.getTresorActiu().toString());
		
		//INTERCANVIEM ELS TRESORS.
		
		Tresor temp1 = new Joguina();
		temp1.clonar(g1.getTresorActiu());
		if (((Joguina)g1.getTresorActiu()).interCanviar(((Joguina)g2.getTresorActiu()))) {
			g1.getTresorActiu().clonar(g2.getTresorActiu());
			g2.getTresorActiu().clonar(temp1);
		}
		
		System.out.println("\nDades dels tresors dels gossos després de fer el canvi: \n");
		System.out.println(g1.getTresorActiu().toString()); 
		System.out.println(g2.getTresorActiu().toString());
		
	}

}
