package capitol5;

public class Joguina extends Tresor implements Intercanviable { 
	private estatJoguina estat;

	@Override
	public String toString() {
		String frase = "\n";
		frase += super.toString();
		return frase + "\nEstat joguina: " + estat;
	}

	public estatJoguina getEstat() {
		return estat;
	}

	public void setEstat(estatJoguina estat) {
		this.estat = estat;
	}
	
	public Joguina() {
		this.estat = estatJoguina.NORMAL;
		this.prioritari = false;
		this.valor = 2;
		this.duracio = 1000;
	}
	
	public Joguina(estatJoguina estat) {
		this.estat = estat;
		this.valor = 6;
		this.duracio = 1000;
		if (this.estat == estatJoguina.NOU)
			this.prioritari = true;
		else
			this.prioritari = false;
	}
	
	public Joguina(boolean prioritari, int valor, int duracio, estatJoguina estat) {
		this.prioritari = prioritari;
		this.valor = valor;
		this.duracio = duracio;
		this.estat = estat;
	}
	
	@Override
	public void enterrar() {
		if (!enterrat) {
			super.enterrar();
			if (this.getEstat() == estatJoguina.NOU && this.getValor()>=1)
				this.setValor(this.getValor()-1);
		}
		
	}
	
	@Override
	public void desenterrar() {
		if (enterrat) {
			super.desenterrar();
			if (this.getValor()>=1)
				this.setValor(this.getValor()-1);
		}
	}
	
	@Override
	public boolean interCanviar(Tresor t) {
		if (t instanceof Intercanviable && t.getValor()>=(this.getValor())*0.75) return true;
		else return false;
	}
}
