package capitol5;

public class Llaminadura extends Tresor implements Comestible {
	private String sabor;

	@Override
	public String toString() {
		String frase = "\n";
		frase += super.toString();
		return frase + "\nSabor: " + sabor;
	}

	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	
	public Llaminadura () {
		this.sabor = "núvol";
		this.prioritari = false;
		this.valor = 2;
		this.duracio = 2;
	}
	
	public Llaminadura(String sabor) {
		this();
		this.sabor = sabor;
	}
	
	public Llaminadura(String sabor, boolean prioritari, int valor, int duracio) {
		this.sabor = sabor;
		this.prioritari = prioritari;
		this.valor = valor;
		this.duracio = duracio;
	}
	
	@Override
	public void enterrar() {
		if (!enterrat) {
			super.enterrar();
			if (this.getValor()>=1)
				this.setValor(this.getValor()-1);
		}
	}
	
	@Override
	public void desenterrar() {
		if (enterrat) {
			super.desenterrar();
			if (this.getValor()>=1) {
				this.setValor(this.getValor()-1);
			}
		}
	}
	
	@Override
	public void clonar(Tresor t) {
		if (t instanceof Llaminadura) {
			super.clonar(t);
			this.sabor = ((Llaminadura)t).sabor;
		}
	}
	
	public String assaborir() {
		return "Mmmmm què bona aquesta llaminadura sabor " + sabor;
	}
	
}
