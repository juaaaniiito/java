package capitol5;

import java.util.ArrayList;
import java.util.Random;

public class Granja {
	private ArrayList<Animal> animals;
	private int numAnimals;
	private int topAnimals;
	
	//CONSTRUCTORS
	public Granja() {
		animals = new ArrayList<Animal>();
		numAnimals = 0;
		topAnimals = 100;
	}
	
	public Granja(int dimensio) {
		if (dimensio <1 || dimensio > 100)
			dimensio = 100;
		topAnimals = dimensio;
		animals = new ArrayList<Animal>();		
	}

	// MÈTODES DE L'OBJECTE
	
	//Métode per afegir un gos
	public int afegir(Animal g) {
		if (numAnimals<topAnimals) {
			animals.add(g);
			numAnimals++;
			return numAnimals;
		} else
			return -1;
	}
	
	//Métode per generar una granja
	public void generarGranja(int totalAnimals) {
		Random rd = new Random();
		char[] sexe = {'M', 'F'};
		String[] races = {"Caniche", "Bulldog", "American Stanford", "Chihuahua", "Podenco", "Pitbull", "Rottweiler", "Pug", "Collie", "Bull Terrier", "Boogle"};
		for (int i = 1; i < totalAnimals+1 && numAnimals<topAnimals; i++) {
			Animal AnimalNou = null;
			int randTipusAnimal = rd.nextInt(2);
			if (randTipusAnimal == 0) { //S'HA DECIDIT PEL RANDOM QUE EL NOU ANIMAL SERÀ UN GOS
				AnimalNou = new Gos();
				numAnimals = afegir(AnimalNou);
				int raçaSioNo = rd.nextInt(2);//CREAR I ASSIGNAR LES RACES
				if (raçaSioNo==1) {
					int raçaRandom = rd.nextInt(11);
					int MidaRandom = rd.nextInt(3);
					GosMida mida;
					if (MidaRandom == 0)
						mida = GosMida.PETIT;
					else if (MidaRandom == 1)
						mida = GosMida.MITJA;
					else
						mida = GosMida.GRAN;
					Raça raçaPerAfegir = new Raça(races[raçaRandom], mida);
					((Gos)obtenirAnimal(i-1)).setRaça(raçaPerAfegir);
				} // RAÇA ASSIGNADA, FALTA ASSIGNAR ESTAT, NOM, FILLS I EDAT(Sexe assignat al constructor Animal()).
				int randEstatAnimal = rd.nextInt(4); 
				if (randEstatAnimal==3) //ASSIGNEM UN 25% DE PROBABILITATS A AFEGIR UN GOS MORT
					AnimalNou.setEstat(AnimalEstat.MORT);
				AnimalNou.setNom("AnimalNum" + i);
				AnimalNou.setFills(rd.nextInt(5));
				AnimalNou.setEdat(rd.nextInt(12));
			} else if (randTipusAnimal == 1) {
				AnimalNou = new Ganso();
				numAnimals = afegir(AnimalNou);
				int randTipusGanso = rd.nextInt(3);
				if (randTipusGanso==0)
					((Ganso)obtenirAnimal(i-1)).setTipus(GansoTipus.AGRESSIU);
				else if (randTipusGanso==1)
					((Ganso)obtenirAnimal(i-1)).setTipus(GansoTipus.DESCONEGUT);
				else
					((Ganso)obtenirAnimal(i-1)).setTipus(GansoTipus.DOMÈSTIC);
				// Com que les caràcterístiques del Ganso són prou diferents, no podem reutilitzar totes les propietats aleàtories que hem fet servir al afegir un gos.
				int randEstatAnimal = rd.nextInt(4); 
				if (randEstatAnimal==3) //ASSIGNEM UN 25% DE PROBABILITATS A AFEGIR UN GANSO MORT
					AnimalNou.setEstat(AnimalEstat.MORT);
				AnimalNou.setNom("AnimalNum" + i);
				AnimalNou.setFills(rd.nextInt(3));
				AnimalNou.setEdat(rd.nextInt(10));
			}
			//ANIMAL CREAT I AFEGIT
		}
		
	}
	
	
	
	//Métode que obté un animal de la granja
	public Animal obtenirAnimal(int posAnimal) {
		if (posAnimal <= animals.size() && posAnimal>=0) {
			return animals.get(posAnimal);
		} else
			return null;
	}
	
	
	//VISUALITZAR la granja
	//Métode per visualitzar tots els animals de la granja
	public void visualitzar() {
		//System.out.println(this.toString());
		int numGos = 1;
		for (int i = 0; i<animals.size(); i++) {
			System.out.println("\nAnimal número " + numGos + ":\n");
			try {
				animals.get(i).visualitzar();
			} catch(NullPointerException e) {
				System.out.println("Animal inexistent");
			}
			
			numGos++;
		}
		System.out.println("\nNÚMERO TOTAL D'ANIMALS: " + numAnimals);
	}
	
	//Métode per visualitzar tots els animals VIUS de la granja
	public void visualitzarVius() {
		//System.out.println(this.toString());
		System.out.println("Dades dels animals vius:\n");
		int nAnimals = 0;
		int nAnimalsVius = 0;
		while (nAnimals<animals.size()) {
			if (animals.get(nAnimals).getEstat() == AnimalEstat.VIU) {
				nAnimalsVius++;
				System.out.println("\nAnimal viu número: " + nAnimalsVius + "\n");
				animals.get(nAnimals).visualitzar();
			}
			nAnimals++;
		}
		System.out.println("NÚMERO TOTAL D'ANIMALS VIUS: " + nAnimalsVius);
	}
	
	@Override
	public String toString() {
		return "Dades de la granja:\n"
				+ "Animals: " + animals + "\nNombre d'animals: " + numAnimals + 
				"\nCapacitat: " + topAnimals + "\n";
	}

	
	// Métodes GETTER
	public int getNumAnimals() {
		return numAnimals;
	}

	public int getTopAnimals() {
		return topAnimals;
	}
	
	
}
