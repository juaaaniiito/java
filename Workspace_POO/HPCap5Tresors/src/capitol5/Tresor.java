package capitol5;

public abstract class Tresor {
	protected boolean prioritari;
	protected int valor;
	protected int duracio;
	protected boolean enterrat;
	
	//GETTERS I SETTERS
	public boolean isPrioritari() {
		return prioritari;
	}
	public int getValor() {
		return valor;
	}
	public int getDuracio() {
		return duracio;
	}
	public boolean isEnterrat() {
		return enterrat;
	}
	public void setPrioritari(boolean prioritari) {
		this.prioritari = prioritari;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public void setDuracio(int duracio) {
		this.duracio = duracio;
	}
	public void setEnterrat(boolean enterrat) {
		this.enterrat = enterrat;
	}

	//MÈTODE toString
	@Override
	public String toString() {
		return "Dades del tresor: \nPrioritari: " + prioritari + "\nValor: " + valor + "\nDuracio: " + duracio + "\nEnterrat: "
				+ enterrat;
	}
	
	//CONSTRUCTORS DE L'OBJECTE 'TRESOR'
	public Tresor() {
		this.prioritari = false;
		this.enterrat = false;
		this.valor = 1;
		this.duracio = 1;
	}
	
	//Com que un constructor "d'Os" només passa 3 valors per paràmetre al constructor de tresor amb super, creo un altre constructor.
	public Tresor(boolean prioritari, int valor, int duracio) {
		this.prioritari = prioritari;
		this.valor = valor;
		this.duracio = duracio;
		//Ho poso per si de cas, alomillor s'ha de canviar o esborrar "POT SER NUL"
		this.enterrat = false;
	}
	
	public Tresor(boolean prioritari, int valor, int duracio, boolean enterrat) {
		this.prioritari = prioritari;
		this.valor = valor;
		this.duracio = duracio;
		this.enterrat = enterrat;
	}
	
	//MÈTODES DE L'OBJECTE 'Tresor'
	
	@Override
	public boolean equals(Object obj) { //TODO Modificació del mètode 'equals' per comparar tresors. 
		//si els dos objectes son el mateix objecte torna true
		if (obj == this)
			return true;
		//si l'objecte passat es null torna false
		if (obj == null)
			return false;
		//si l'objecte passat es d'una classe diferent
		if (this.getClass() != obj.getClass())
			return false;
		
		//els dos tresors tenen el mateix valor i els dos estan enterrats o desenterrats
		if (this.valor == ((Tresor)obj).valor)
			if ((this.enterrat && ((Tresor)obj).enterrat) || (!this.enterrat && !((Tresor)obj).enterrat))
				return true;
		
		//Assignació del màxim i del mínim
		int maxim, minim;
		maxim = this.valor;
		minim = this.valor;
		if (this.valor > ((Tresor)obj).valor)
			minim = ((Tresor)obj).valor;
		else if (this.valor < ((Tresor)obj).valor)
			maxim = ((Tresor)obj).valor;
		
		//la diferència de valor entre ells és de dos com a màxim, són del mateix tipus, i el de menys valor com a mínim té valor 1
		if(maxim-minim <= 2 && obj.getClass() == this.getClass() && minim >=1)
			return true;
		
		
		
		return false;
	}
	
	public void enterrar() {
		this.enterrat = true;
	}
	
	public void desenterrar() {
		this.enterrat = false;
	}
	
	public void clonar(Tresor t) {
		this.prioritari = t.prioritari;
		this.valor = t.valor;
		this.duracio = t.duracio;
		this.enterrat = t.enterrat;
	}
	
	
}
