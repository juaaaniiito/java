package apartat3A;


public class Programa {

	public static void main(String[] args) {
		//Hacer un programa que pruebe las nuevas clases 'gos' i 'ganso'
		for (int i = 0; i < 15; i++) {
			if (i<5) {
				Animal g = new Animal();
				g.visualitzar();
				g.so();
				g.incEdat();
				System.out.println(Animal.getNumAnimals());
			} else if (i>=5 && i<10) {
				Animal g = new Gos();
				g.visualitzar();
				g.so();
				g.incEdat();
				System.out.println(Animal.getNumAnimals());
			} else {
				Animal g = new Ganso();
				g.visualitzar();
				g.so();
				g.incEdat();
				System.out.println(Animal.getNumAnimals());
			}
		}
		/*
		Animal g1 = new Gos();
		g1.setEdat(6);
		g1.setEstat(AnimalEstat.VIU);
		g1.setNom("Kenu");
		Raça r1 = new Raça("Pitbull", Gos.GosMida.GRAN);
		((Gos)g1).setRaça(r1); // TODO Hem de castear l'objecte ja que l'hem creat com si fos un animal però no sap que ara és un gos, per poder fer servir la seva raça
		g1.visualitzar();
		Animal g2 = new Gos();
		g2.setEdat(5);
		g2.setSexe(AnimalSexe.M);
		Animal g3 = g1.aparellar(g2);
		if (g3 instanceof Gos) {
			g3.incEdat();
			g3.visualitzar();
			g3.so();
		}
		*/
		
	}

}
