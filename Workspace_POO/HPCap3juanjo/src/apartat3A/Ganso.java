package apartat3A;

public class Ganso extends Animal{
	private GansoTipus tipus;
	private static int edatTope = 6;
	
	Ganso() {
		super();
		edatTope = 6;
		setTipus(GansoTipus.DESCONEGUT);
	}
	
	Ganso(String nom, int edat, GansoTipus tipus, char sexe){
		super(nom, edat, sexe);
		this.setTipus(tipus);
	}
	
	Ganso (String n){
		super(n);
		setTipus(GansoTipus.DESCONEGUT);
	}
	
	Ganso(Ganso g) {
		this();
		clonar(g);
	}
	
	public Ganso(int edat, String nom, int fills, AnimalSexe sexe, AnimalEstat estat, GansoTipus tipus2) {
		super(edat, nom, fills, sexe, estat);
		this.tipus = tipus2;
	}

	//MÈTODES DE L'OBJECTE PER SOBREESCRIURE 'Animal.java'
	@Override
	public void so() {
		System.out.println("Honk Honk");
	}
	
	@Override
	public void clonar(Animal a) {
		if (!(((Ganso)a) instanceof Animal)) {
			super.clonar(a);
			this.tipus = ((Ganso)a).tipus;
		}
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		cadena += "\nTipus Ganso: " + tipus;
		return cadena;
	}
	@Override
	public void visualitzar() {
		System.out.println("Dades del ganso: ");
		System.out.println(this.toString());
		System.out.println("//////////////////////");
	}
	
	@Override
	public void incEdat() {
		super.incEdat();
		if (this.getTipus() == GansoTipus.AGRESSIU)
			if (this.getEdat() >= edatTope-1)
				this.setEstat(AnimalEstat.MORT);
		else if (this.getTipus() == GansoTipus.DOMÈSTIC) 
			if (this.getEdat() >= edatTope+1) 
				this.setEstat(AnimalEstat.MORT);
		else if (this.getTipus() == GansoTipus.DESCONEGUT)
			if (this.getEdat()>= edatTope)
				this.setEstat(AnimalEstat.MORT);
	}
	
	@Override
	public Animal aparellar(Animal nuvi) {
		Animal fill = null;
		
		if (!(nuvi instanceof Ganso)) return null;
		
		fill = super.aparellar(nuvi); // En aquest moment, està intentant aparellar entre el 
									  //ganso que ha cridat AQUEST mètode, i el que entra per paràmetre
		
		if (fill == null) return null;
		
		return new Ganso(fill.edat, fill.nom, fill.fills, fill.sexe, fill.estat, this.tipus);
	}
	
	
	public GansoTipus getTipus() {
		return tipus;
	}

	public void setTipus(GansoTipus tipus) {
		this.tipus = tipus;
	}

	enum GansoTipus{
		DESCONEGUT, AGRESSIU, DOMÈSTIC
	}
}
