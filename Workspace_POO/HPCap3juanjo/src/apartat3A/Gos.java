package apartat3A;

public class Gos extends Animal {
	private Raça raça;
	
	//CONSTRUCTORS:
	/*Constructor sense paràmetres, per defecte.*/
	public Gos() {
		super();
		raça = null;
	}
	
	public Gos(int edat, String nom, int numFills, AnimalSexe sexe, AnimalEstat estat, Raça raça) {
		super(edat, nom, numFills, sexe, estat);
		this.raça = raça;

	}
	
	/*Constructor que rep tots els valors per paràmetre*/
	public Gos(String nom, int edat, Raça raça, char sexe) {
		super(nom, edat, sexe);
		this.raça = raça;
	}
	
	
	
	public Gos (String n) {
		super(n);
		this.raça=null;
	}
	
	
	/*Constructor que còpia els valors d'un altre gos.*/
	public Gos(Gos g) {
		this();
		clonar(g);
	}
	
	

	//MÈTODES DE L'OBJECTE PER SOBREESCRIURE 'Animal.java'
	@Override
	public void visualitzar() {
		System.out.println("Dades del gos:");
		System.out.println(this.toString());
		System.out.println("/////////////////");
	}
	
	@Override
	public String toString() {
		String cadena = super.toString();
		if (this.raça != null)
			cadena += "\nRaça: " + raça.getNomRaça();
		else
			cadena += "\nRaça: Desconeguda";
		return cadena;
	}
	
	@Override
	public void clonar(Animal a) {
		if (a instanceof Gos) {
			super.clonar(a);
			this.raça = ((Gos)a).raça;
		}
	}
	
	
	@Override
	public void so() {
		System.out.println("guau guau");
	}
	
	@Override
	public void incEdat() {
		super.incEdat();
		if (this.raça == null && this.getEdat()>=10) {
			this.setEstat(AnimalEstat.MORT);
		} else if (this.raça != null && this.getEdat() > this.raça.getTempsVida()) {
			this.setEstat(AnimalEstat.MORT);
		}
	}
	
	
	@Override
	public Animal aparellar(Animal a) {
		Animal fill = null;
		
		if (!(a instanceof Gos)) return null;
		
		fill = super.aparellar(a);
		
		if (fill == null) return null;
		
		return new Gos(fill.edat, fill.nom, fill.fills, fill.sexe, fill.estat, this.raça);
	}
	
	//Enumeracions de les mides i estats dels gossos
	public enum GosMida {
		GRAN, MITJA, PETIT
	}
	
	public enum GosEstat{
		VIU, MORT
	}
	
	public Raça getRaça() {
		return raça;
	}
	
	public void setRaça(Raça raça) {
		this.raça = raça;
	}

}

	