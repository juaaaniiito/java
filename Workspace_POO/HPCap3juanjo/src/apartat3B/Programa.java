package apartat3B;

public class Programa {

	public static void main(String[] args) {
		Granja granja1 = new Granja();
		granja1.generarGranja(20);
		Granja granja2 = new Granja();
		granja2.generarGranja(20);
		Granja granjaNous = new Granja();
		for (int i = 0; i < granja1.getNumAnimals(); i++) {
			try {
				granjaNous.afegir(granja1.obtenirAnimal(i).aparellar(granja2.obtenirAnimal(i)));
			} catch(Exception e) {
				System.out.println("No ha sorgit l'amor i no s'ha afegit cap animal a la novaGranja " + i);
			}
		}
		System.out.println("\nVISUALITZEM GRANJA 1");
		granja1.visualitzar();
		System.out.println("\nVISUALITZEM GRANJA 2");
		granja2.visualitzar();
		System.out.println("\nVISUALITZEM GRANJA 3");
		granjaNous.visualitzar();
	}

}
