package capitol1;

public class Raça {
	
	private String nomRaça;
	private Gos.GosMida mida;
	private int tempsVida = 10;
	private boolean dominant;
	
	public Raça(String nom, Gos.GosMida gt, int t) {
		this(nom, gt);
		this.tempsVida = t;
	}
	
	public Raça(String nom, Gos.GosMida mida) {
		this.nomRaça = nom;
		this.mida = mida;
		this.tempsVida = 10;
		this.dominant = false;
	}
	
	//Mètode toString
	@Override
	public String toString() {
		return nomRaça + ", mida: " + mida
				+ ", tempsVida: " + tempsVida + ", dominant: " + dominant;
	}
	
	//GETTERS I SETTERS
	public String getNomRaça() {
		return nomRaça;
	}

	public void setNomRaça(String nomRaça) {
		this.nomRaça = nomRaça;
	}

	public Gos.GosMida getMida() {
		return mida;
	}

	public void setMida(Gos.GosMida mida) {
		this.mida = mida;
	}

	public int getTempsVida() {
		return tempsVida;
	}

	public void setTempsVida(int tempsVida) {
		if (tempsVida>=0)
			this.tempsVida = tempsVida;
	}

	public boolean isDominant() {
		return dominant;
	}

	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}
	
	
	
	
}
