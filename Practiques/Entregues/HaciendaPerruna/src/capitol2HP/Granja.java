package capitol2HP;

import java.util.ArrayList;
import java.util.Random;

public class Granja {
	private ArrayList<Gos> gossos;
	private int numGossos;
	private int topGossos;
	
	//CONSTRUCTORS
	public Granja() {
		gossos = new ArrayList<Gos>();
		numGossos = 0;
		topGossos = 100;
	}
	
	public Granja(int dimensio) {
		if (dimensio <1 || dimensio > 100)
			dimensio = 100;
		topGossos = dimensio;
		gossos = new ArrayList<Gos>();		
	}

	// MÈTODES DE L'OBJECTE
	
	//Métode per afegir un gos
	public int afegir(Gos g) {
		if (numGossos<topGossos) {
			gossos.add(g);
			numGossos++;
			return numGossos;
		} else
			return -1;
	}
	
	//Métode per generar una granja
	public void generarGranja(int totalGossos) {
		Random rd = new Random();
		char[] sexe = {'M', 'F'};
		String[] races = {"Caniche", "Bulldog", "American Stanford", "Chihuahua", "Podenco", "Pitbull", "Rottweiler", "Pug", "Collie", "Bull Terrier", "Boogle"};
		for (int i = 1; i < totalGossos+1 && numGossos<topGossos; i++) {
			Gos gosPerAfegir = new Gos();
			numGossos = afegir(gosPerAfegir);
			int raçaSioNo = rd.nextInt(2);//CREAR I ASSIGNAR LES RACES
			if (raçaSioNo==1) {
				int raçaRandom = rd.nextInt(11);
				int MidaRandom = rd.nextInt(3);
				Gos.GosMida mida;
				if (MidaRandom == 0)
					mida = Gos.GosMida.PETIT;
				else if (MidaRandom == 1)
					mida = Gos.GosMida.MITJA;
				else
					mida = Gos.GosMida.GRAN;
				Raça raçaPerAfegir = new Raça(races[raçaRandom], mida);
				obtenirGos(i-1).setRaça(raçaPerAfegir);
			}
			gosPerAfegir.setEdat(rd.nextInt(11));
			gosPerAfegir.setNom("g" + i);
			gosPerAfegir.setFills(rd.nextInt(5));
			gosPerAfegir.setSexe(sexe[rd.nextInt(2)]);
			//GOS CREAT I AFEGIT
		}
		
	}
	
	//Métode que mostra per cada gos viu de la granja els seus pares i els seus fills.
	public void arbreGenealogic() {
		for (int i = 0; i < this.getNumGossos(); i++) {// Per cada gos de la granja
			if (this.obtenirGos(i).getEstat() == Gos.GosEstat.VIU) { //Si el gos està viu:
				System.out.println("--DADES DEL GOS " + this.obtenirGos(i).getNom() + "--");
				Gos Pare = this.obtenirGos(i).getPare();
				Gos Mare = this.obtenirGos(i).getMare();
				if (Pare != null)
					System.out.println("El seu pare és: " + Pare.getNom());
				if (Mare != null)
					System.out.println("La seva mare és: " + Mare.getNom());
				ArrayList<Gos> fillsGos = this.obtenirGos(i).fills(this);
				if (fillsGos.size() > 0) {
					System.out.println("I té els següents fills: ");
					for (int j = 1; j < fillsGos.size(); j++) {
						System.out.println("\n Fill nº" + j + " " + fillsGos.get(j-1).getNom());
					}
				}
				if(Pare == null && Mare == null && fillsGos.size() == 0) {
					System.out.println("El gos " + this.obtenirGos(i).getNom() + " no té pare, ni mare ni fills.");
				}
			}
		}
		
		
	}
	
	//Métode que elimina un gos de la granja
	public void eliminarGos(int posicio) {
		this.gossos.remove(posicio);
		this.numGossos = numGossos -1;
	}
	
	//Métode que obté un gos de la granja
	public Gos obtenirGos(int posGos) {
		if (posGos <= gossos.size() && posGos>=0) {
			return gossos.get(posGos);
		} else
			return null;
	}
	
	
	//VISUALITZAR la granja
	//Métode per visualitzar tots els gossos de la granja
	public void visualitzar() {
		//System.out.println(this.toString());
		int numGos = 1;
		for (int i = 0; i<gossos.size(); i++) {
			System.out.println("\nGos número " + numGos + ":\n");
			gossos.get(i).visualitzar();
			numGos++;
		}
		System.out.println("NÚMERO TOTAL DE GOSSOS: " + numGossos);
	}
	
	//Métode per visualitzar tots els gossos VIUS de la granja
	public void visualitzarVius() {
		//System.out.println(this.toString());
		System.out.println("Dades dels gossos vius:\n");
		int nGossos = 0;
		int nGossosVius = 0;
		while (nGossos<gossos.size()) {
			if (gossos.get(nGossos).getEstat() == Gos.GosEstat.VIU) {
				nGossosVius++;
				System.out.println("\nGos viu número: " + nGossosVius + "\n");
				gossos.get(nGossos).visualitzar();
			}
			nGossos++;
		}
		System.out.println("NÚMERO TOTAL DE GOSSOS VIUS: " + nGossosVius);
	}
	
	@Override
	public String toString() {
		return "Dades de la granja:\n"
				+ "Gossos: " + gossos + "\nNombre de gossos: " + numGossos + 
				"\nCapacitat: " + topGossos + "\n";
	}

	
	// Métodes GETTER
	public int getNumGossos() {
		return numGossos;
	}

	public int getTopGossos() {
		return topGossos;
	}
	
	
}
