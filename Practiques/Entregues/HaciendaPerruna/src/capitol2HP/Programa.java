package capitol2HP;

import java.util.Random;

class Programa {
	static Random rd = new Random();
	
	public static void main(String[] args) {
		Granja granjaJuanjo = new Granja();
		granjaJuanjo.generarGranja(12);
		System.out.println("Granja generada amb 12 gossos aleatoris.");
		granjaJuanjo.visualitzar();
		Granja cementiri = new Granja();
		for (int i = 0; i<10; i++) { // En un període de 10 anys, per cada any
			int errades = 0;
			while (errades < 5) {
				int rand1 = rd.nextInt(granjaJuanjo.getNumGossos());
				int rand2 = rd.nextInt(granjaJuanjo.getNumGossos());
				while(rand1 == rand2) {
					rand2 = rd.nextInt(granjaJuanjo.getNumGossos());
				}
				Gos g1 = granjaJuanjo.obtenirGos(rand1);
				Gos g2 = granjaJuanjo.obtenirGos(rand2);
				Gos g3;
				g3 = g1.aparellar(g2);
				if (g3 == null) {
					errades++;
				} else {
					granjaJuanjo.afegir(g3);
				}
			}
			for (int j = 0; j < granjaJuanjo.getNumGossos(); j++) { // Per cada gos
				granjaJuanjo.obtenirGos(j).incEdat();
				if (granjaJuanjo.obtenirGos(j).getEstat() == Gos.GosEstat.MORT) {
					// AFEGEIXO EL MÉTODE eliminarGos(int) a la classe 'Granja.java'
					cementiri.afegir(granjaJuanjo.obtenirGos(j));
					granjaJuanjo.eliminarGos(j);
				}
			}
			System.out.println("\nFINAL DEL CICLE.\n");
			System.out.println("------------------------");
			System.out.println("---DADES DE LA GRANJA---");
			System.out.println("------------------------");
			granjaJuanjo.visualitzar();
			System.out.println("------------------------");
			System.out.println("---DADES DEL CEMENTIRI--");
			System.out.println("------------------------");
			cementiri.visualitzar();
			
		}
		
		granjaJuanjo.arbreGenealogic();
		
	}
	
}
