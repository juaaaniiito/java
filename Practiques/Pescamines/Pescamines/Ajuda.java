package Pescamines;
/**
 * Mostra l'ajuda i informació general del joc.
 * @author Juan José Rodríguez
 *
 */
public class Ajuda {
/**
 * Llistat de regles del joc.
 */
	public static void MostrarAjuda() {
		System.out.println("\nBENVINGUT AL JOC DEL PESCAMINES\n");
		System.out.println("Regles del joc:");
		System.out.println("- El nom del jugador no pot contenir espais.");
		System.out.println("- Al mode de joc personalitzat, el numero de mines ha de ser com màxim un terç dels espais totals.");
		System.out.println("Joc desenvolupat per Juan José Rodríguez");
	}

}
