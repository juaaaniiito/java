package Pescamines;

import java.util.Random;
import java.util.Scanner;
/**
 * Prepara i desenvolupa el joc.
 * @author Juan José Rodríguez
 */
public class Jugar {

	static Scanner src = new Scanner(System.in);
	static Random rd = new Random();
	public static char[][] tauler = new char[0][0];
	public static char[][] tauler_darrere = new char[0][0];
/**
 * Distribuïdor de mètodes per jugar la partida.
 * @return Retorna el resultat de la partida(Victoria o Derrota).
 */
	public static String JugarPartida() {
		String nom_jugador = Opcions.nom_jugador;
		OmplirTaulers(Opcions.files, Opcions.columnes, Opcions.numero_mines);
		String joc_acabat = "";
		do {
			MostrarSituacio(tauler, nom_jugador);
			joc_acabat = RebreTirada(Opcions.files, Opcions.columnes);
		} while (!joc_acabat.equals("victoria") && !joc_acabat.equals("derrota"));
		
		if (joc_acabat.equals("victoria")) {
			System.out.println("Enhorabona, has guanyat!!");
			MostrarSituacio(tauler, nom_jugador);
		}
		else {
			System.out.println("Si aprens de la derrota, llavors no és una derrota.");
		}
		return joc_acabat;
	}
/**
 * Rep les coordenades de les tirades de l'usuari i fa la comprovació del final del joc.
 * @param files Files del tauler del joc.
 * @param columnes Columnes del tauler del joc.
 * @return Retorna el resultat de la partida(Victoria o Derrota).
 */
	private static String RebreTirada(int files, int columnes) {
		String resultat = ""; 
		System.out.println("COORDENADES DE LA TIRADA:");
		System.out.print("Introdueix el numero de fila: ");
		int posicioY = Tirada(files);
		System.out.print("Introdueix el numero de columna: ");
		int posicioX = Tirada(columnes);
		resultat = Comprovacio(posicioY, posicioX, files, columnes);
		return resultat;
	}
/**
 * Comprovació de les coordenades que rep per l'usuari comparant el tauler que mostra amb el que hi ha per darrere.
 * @param posicioY Valor de la coordenada vertical de l'usuari.
 * @param posicioX Valor de la coordenada horitzontal de l'usuari.
 * @param files Files del tauler del joc.
 * @param columnes Columnes del tauler del joc.
 * @return Retorna el resultat de la partida(Victoria o Derrota).
 */
	private static String Comprovacio(int posicioY, int posicioX, int files, int columnes) {
		String resultat ="";
		if (tauler_darrere[posicioY][posicioX]=='*') {
			tauler[posicioY][posicioX] = tauler_darrere[posicioY][posicioX];
			resultat = "derrota";
			return resultat;
		} else if (tauler_darrere[posicioY][posicioX]!='0' && tauler_darrere[posicioY][posicioX]!='*') {
			tauler[posicioY][posicioX] = tauler_darrere[posicioY][posicioX];
			resultat = ComprovarVictoria();
		}else if (tauler_darrere[posicioY][posicioX]=='0') {
			tauler[posicioY][posicioX] = '0';
			destaparNums(posicioY, posicioX, files, columnes);
		}
		if (!resultat.equals("victoria") || (!resultat.equals("derrota"))) {
			resultat = ComprovarVictoria();
		}
		return resultat;
	}
/**
 * Pinta els numeros del tauler que es mostra a l'usuari amb un mètode recursiu.
 * @param posicioY Valor de la coordenada vertical de l'usuari.
 * @param posicioX Valor de la coordenada horitzontal de l'usuari.
 * @param files Files del tauler del joc.
 * @param columnes Columnes del tauler del joc.
 */
	private static void destaparNums(int posicioY, int posicioX, int files, int columnes) {
		if (posicioY-1>=0) {
			if (tauler_darrere[posicioY-1][posicioX]!='0') {
				tauler[posicioY-1][posicioX]=tauler_darrere[posicioY-1][posicioX];
			} else if (tauler_darrere[posicioY-1][posicioX]=='0' && tauler[posicioY-1][posicioX]!='0') {
				tauler[posicioY-1][posicioX]='0';
				destaparNums(posicioY-1, posicioX, files, columnes);
			}
		}
		
		if (posicioX-1>=0) {
			if (tauler_darrere[posicioY][posicioX-1]!='0') {
				tauler[posicioY][posicioX-1]=tauler_darrere[posicioY][posicioX-1];
			} else if (tauler_darrere[posicioY][posicioX-1]=='0'&&tauler[posicioY][posicioX-1]!='0') {
				tauler[posicioY][posicioX-1]='0';
				destaparNums(posicioY, posicioX-1, files, columnes);
			}
		}
		
		if (posicioX+1<columnes) {
			if (tauler_darrere[posicioY][posicioX+1]!='0') {
				tauler[posicioY][posicioX+1]=tauler_darrere[posicioY][posicioX+1];
			} else if (tauler_darrere[posicioY][posicioX+1]=='0'&&tauler[posicioY][posicioX+1]!='0') {
				tauler[posicioY][posicioX+1]='0';
				destaparNums(posicioY, posicioX+1, files, columnes);
			}
		}
		
		if (posicioY+1<files) {
			if (tauler_darrere[posicioY+1][posicioX]!='0') {
				tauler[posicioY+1][posicioX]=tauler_darrere[posicioY+1][posicioX];
			} else if (tauler_darrere[posicioY+1][posicioX]=='0'&&tauler[posicioY+1][posicioX]!='0') {
				tauler[posicioY+1][posicioX]='0';
				destaparNums(posicioY+1, posicioX, files, columnes);
			}
		}
	}
/**
 * Comprova els dos taulers per saber si l'usuari ha guanyat.
 * @return En cas de que l'usuari ha guanyat, retorna "victoria", en cas contrari retorna: "".
 */
	private static String ComprovarVictoria() {
		String resultat="victoria";
		for (int i = 0; i < Opcions.files; i++)
			for (int j = 0; j<Opcions.columnes; j++)
				if(tauler_darrere[i][j]!='*') {
					if (tauler_darrere[i][j]!= tauler[i][j]) {
						resultat="";
					}
				} 
		return resultat;
	}
/**
 * Valida els valors de les coordenades que entra l'usuari.
 * @param files_o_columnes Valor de fila o columna que entra l'usuari.
 * @return Retorna el valor validat segons correspongui.
 */
	private static int Tirada(int files_o_columnes) {
		boolean numValid = false;
		boolean numero = false;
		int num = -1;
		do {
			int errors=0;
			numero = false;
			try {
				num = src.nextInt();
				src.nextLine();
				numero=true;
			}
			catch (Exception e) {
				System.out.println("Error, has d'introduir un valor enter.");
				src.nextLine();
				errors++;
			}
			if (num>=0 && num<=files_o_columnes) {
				numValid=true;
			} else if (numero){
				System.out.println("Error, has d'introduir un valor vàlid.");
				errors++;
			}
			if(errors>0)
				System.out.print("Torna-ho a provar: ");
		}while (!numValid);
		return num-1;
	}
/**
 * Omple el tauler amb mines i assigna els numeros del tauler de darrere.
 * @param files Valor màxim de files del tauler.
 * @param columnes Valor màxim de columnes del tauler.
 * @param numero_mines Numero de mines per distribuïr al tauler.
 */
	private static void OmplirTaulers(int files, int columnes, int numero_mines) {
		tauler= new char[files][columnes];
		tauler_darrere= new char[files][columnes];
		for (int i = 0; i<files; i++) {
			for (int j = 0; j<columnes; j++) {
				tauler[i][j] = '-';
				tauler_darrere[i][j] = '0'; //Corregir para mirar con MostrarSituacio()
			}
		}
		FicarMines(files, columnes, numero_mines);
		AssignarNumeros(files, columnes);
	}	

/**
 * Assigna els numeros de les caselles segons les mines que tingui al voltant.
 * @param files Valor màxim de files del tauler.
 * @param columnes Valor màxim de columnes del tauler.
 */
	private static void AssignarNumeros(int files, int columnes) {
		for (int i = 0; i<files;i++) {
			for (int j = 0; j<columnes; j++) {
				if (tauler_darrere[i][j]=='0') {
					int num = VerificarCasella(i, j, files, columnes);
					char num_char2 = (char) (num+'0'); //TODO Pasar de int a char
					tauler_darrere[i][j]= num_char2;
				}
				//System.out.print(tauler_darrere[i][j]+ " ");
			}
		//System.out.println();
		}
	}
/**
 * Compta les mines que hi ha al voltant d'una coordenada.
 * @param i Fila actual de la coordenada.
 * @param j Columna actual de la coordenada.
 * @param files Valor màxim de files del tauler.
 * @param columnes Valor màxim de columnes del tauler.
 * @return Retorna el numero de mines que hi ha al voltant. 
 */
	private static int VerificarCasella(int i, int j, int files, int columnes) {
		int num_bombes =0;
		for (int p = i-1; p<i+2; p++)
			for (int h = j-1; h<j+2; h++)
				if (DintreTaulell(p, h, files, columnes)) {
					if (tauler_darrere[p][h]=='*')
						num_bombes++;
				}
		return num_bombes;
	}
/**
 * Verifica que la casella que comprova la mina estigui dins de la matriu.
 * @param i Fila actual de la coordenada.
 * @param j Columna actual de la coordenada.
 * @param files Valor màxim de files del tauler.
 * @param columnes Valor màxim de columnes del tauler.
 * @return Retorna un booleà si la coordenada està dins de la matriu o no.
 */
	private static boolean DintreTaulell(int i, int j, int files, int columnes) {
		boolean dintre= false;
		if (i>=0 && i <files && j >=0 && j<columnes)
			dintre = true;
		return dintre;
	}
/**
 * Fica les mines aleatòriament en el tauler no visible per l'usuari.
 * @param files Files del tauler del joc.
 * @param columnes Columnes del tauler del joc.
 * @param numero_mines Numero de mines que s'han de col·locar al tauler.
 */
	private static void FicarMines(int files, int columnes, int numero_mines) {
		int mines_per_posar = numero_mines;
		while(mines_per_posar>0) {
			boolean PosMinaCorrecte = false;
			do {
				int fil_mina = rd.nextInt(0,files);
				int col_mina = rd.nextInt(0,columnes);
				if (tauler_darrere[fil_mina][col_mina]=='0') {
					tauler_darrere[fil_mina][col_mina]='*';
					PosMinaCorrecte=true;
				}
			}while(!PosMinaCorrecte);
			mines_per_posar=mines_per_posar-1;
		}
	}
/**
 * Mostra l'estat actual del tauler del jugador per pantalla. A més del nom actual del jugador.
 * @param tauler Matriu del tauler que mostra per pantalla.
 * @param nom_jugador Nom del jugador actiu.
 */
	private static void MostrarSituacio(char[][] tauler, String nom_jugador) {
		System.out.print("\nJugador actual: " + nom_jugador +"\n");
		System.out.print("     ");
		for (int i = 0; i < tauler[0].length; i++) {
			System.out.printf("%3d", i+1);
		}
		System.out.println();
		System.out.print("    _");
		for (int i = 0; i < tauler[0].length; i++) {
			System.out.printf("%3c", '_');
		}
		System.out.println();
		for (int i = 0; i < tauler.length; i++) {
			if (i>=9 && i<100)
				System.out.print((i+1) + " | ");
			if (i>-1 && i<9)
				System.out.print((i+1) + "  | ");
			for (int j = 0; j < tauler[i].length; j++) {
				System.out.printf("%3c", tauler[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}


}

