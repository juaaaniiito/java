package Pescamines;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe que determina el valors dels paràmetres del joc, a més d'actualitzar-les quan canvien.
 * @author Juan José Rodríguez
 *
 */
public class Opcions {
	
	static Scanner src = new Scanner(System.in);
	public static int numero_mines = 0;
	public static int files = 0;
	public static int columnes = 0;
	public static String nom_jugador;
	static ArrayList<String> nom_jugadors = Programa.nom_jugadors;
	static ArrayList<Integer> victories = Programa.victories;
	static ArrayList<Integer> derrotes = Programa.derrotes;

/**
 * Crida als mètodes SeleccionarJugador i SeleccionarDificultat
 */
	public static void DefinirPartida() {
		SeleccionarJugador(nom_jugadors, victories, derrotes);
		SeleccionarDificultat();
	}
/**
 * Estableix la dificultat del joc segons l'elecció de l'usuari cridant altres mètodes.
 */
	private static void SeleccionarDificultat() {
		char opcio;
		opcio = MostrarMenuDificultat();
		switch (opcio) {
		case ('1'):
			files = 8;
			columnes = 8;
			numero_mines = 10;
			break;
		case ('2'):
			files = 16;
			columnes = 16;
			numero_mines=10;
			break;
		case ('3'):
			files = 16;
			columnes = 30;
			numero_mines=99;
			break;
		case ('4'):
			CrearTaulerPersonalitzat();
		}
	}
/**
 * Mostra les opcions disponibles per la dificultat del joc.
 * @return Retorna l'opció escollida per l'usuari.
 */
	private static char MostrarMenuDificultat() {
		boolean valid = false;
		char opcio;
		do {
			System.out.println("\nEscull una dificultat:\n");
			System.out.println("1. Principiant.");
			System.out.println("2. Intermedi.");
			System.out.println("3. Expert.");
			System.out.println("4. Personalitzat");
			opcio = src.next().charAt(0);
			if (opcio=='1' || opcio=='2' || opcio == '3'|| opcio =='4')
				valid=true;
			else
				System.out.println("Has d'escollir una opció vàlida.");
		} while (!valid);
		return opcio;
	}
/**
 * Determina els valors per les files, columnes i mines del tauler del joc.
 */
	private static void CrearTaulerPersonalitzat() {
		//Comprovar nombre enter();
		files=0;
		columnes=0;
		numero_mines=0;
		System.out.print("Introdueix el numero de files: ");
		files = ValorEnter();
		System.out.print("\nIntrodueix el numero de columnes: ");
		columnes = ValorEnter();
		System.out.print("\nIntrodueix el numero de mines: ");
		numero_mines = ValorEnter();
	}
/**
 * Comprova que el valor que entra l'usuari és vàlid i no causarà problemes en l'execució del joc
 * @return Retorna un valor enter entrat per l'usuari.
 */
	private static int ValorEnter() {
		boolean num_valid = false;
		int enter=0;
		if (files>0 && columnes>0) {
			do {
				try {
					enter = src.nextInt();
					src.nextLine();
					num_valid = true;
				}
				catch (Exception e) {
					System.out.println("Error, has introduit un valor incoherent, torna-ho a provar.");
					src.nextLine();
				}
				if(enter > (files*columnes)/3)
					System.out.print("\nIntrodueix un menor numero de mines(Llegir ajuda): ");
				if(enter <= 0)
					System.out.print("\nIntrodueix un valor positiu: ");
			} while (!num_valid || enter<=0 || enter > (files*columnes)/3);
		} else {
			do {
				try { // Catch per verificar l'entrada d'un numero enter
					enter = src.nextInt();
					src.nextLine();
					num_valid = true;
				}
				catch (Exception e) {
					System.out.println("Error, has d'introduir un valor enter i major a 0.");
					src.nextLine();
				}
				if(enter <= 0)
					System.out.print("\nIntrodueix un valor positiu: ");
			} while (!num_valid || enter<0);
		}
		return enter;
	}
/**
 * Insereix informació a llistes referents a les victories, derrotes i noms dels jugadors.
 * @param nom_jugadors Llista on s'emmagatzemen els noms dels jugadors
 * @param victories Llista on s'emmagatzemen les victories dels jugadors
 * @param derrotes Llista on s'emmagatzemen les derrotes dels jugadors
 */
	private static void SeleccionarJugador(ArrayList<String> nom_jugadors, ArrayList<Integer> victories, ArrayList<Integer> derrotes) {
		String opcio;
		boolean escollit = false;
		do {
			System.out.print("\nVols crear un nou jugador? ");
			opcio= src.next().toLowerCase();
			if (opcio.equals("si") || opcio.equals("no")) {
				if (opcio.equals("si")) {
					System.out.print("\nEscriu el nom del nou jugador: ");
					nom_jugador = src.next();
					if (nom_jugadors.contains(nom_jugador)) {
						System.out.println("Aquest nom ja existeix, escull un altre.");
					} else {
						System.out.println("Creant i seleccionant jugador...");
						nom_jugadors.add(nom_jugador);
						victories.add(0);
						derrotes.add(0);
						escollit = true;
					}
				}
				if (opcio.equals("no")) {
					System.out.print("\nEscriu el nom del jugador existent: ");
					nom_jugador = src.next();
					if (nom_jugadors.contains(nom_jugador)) {
						System.out.println("Seleccionant jugador existent...");
						escollit = true;
					} else {
						System.out.println("Aquest jugador no existeix.\nAssegura't d'escrire-ho bé.");
					}
				}
			} else
				System.out.println("Escriu 'si' o 'no'.");
		} while(!escollit);
	}
/**
 * Actualitza les victories i derrotes dels jugadors quan s'acaba una partida.
 * @param resultat Indica si el jugador actual ha guanyat o ha perdut la partida.
 */
	public static void actualitzarResultats(String resultat) {
		int posicioJugador = nom_jugadors.indexOf(nom_jugador);
		for (int i =0; i<nom_jugadors.size(); i++) {
			if (nom_jugadors.get(i).equals(nom_jugador)) {
				if (resultat.equals("victoria")) {
					victories.set(i, victories.get(i)+1);
				} else if (resultat.equals("derrota")) {
					derrotes.set(i, derrotes.get(i)+1);
				}
			}
		}
	}
/**
 * Mostra per pantalla el Ranking dels jugadors.
 */
	public static void mostrarRanking() {
		for (int i =0; i<nom_jugadors.size(); i++) {
			System.out.print("\nEl jugador " + nom_jugadors.get(i) + " té " + victories.get(i) + " victories, i " + derrotes.get(i) + " derrotes.");
		}
		System.out.println();
	}
	
}
