package Pescamines;

import java.util.ArrayList;
import java.util.Scanner;
/**
 * Programa servirà com un menu distribuidor de tasques entre les diferents classes(Jugar, Opcions i Ajuda).
 * @author Juan José Rodríguez
 * 
 */
public class Programa {
	
	static Scanner src = new Scanner(System.in);
	public static ArrayList<String> nom_jugadors = new ArrayList<String>();
	public static ArrayList<Integer> victories = new ArrayList<Integer>();
	public static ArrayList<Integer> derrotes = new ArrayList<Integer>();

	/**
	 * Menu principal distribuidor de mètodes
	 * @param args
	 */
	public static void main(String[] args) {
		boolean acabat = false;
		char opcio;
		boolean joc_definit = false;
		String resultat;
		do {
			opcio = MostrarMenu();
			switch (opcio) {
			case('1'):
				Ajuda.MostrarAjuda();
				break;
			case('2'):
				Opcions.DefinirPartida();
				joc_definit = true;
				break;
			case('3'):
				if (!joc_definit)
					System.out.println("Primer has de definir la partida a la opció 2.");
				else {
					resultat = Jugar.JugarPartida();
					Opcions.actualitzarResultats(resultat);
				}
				break;
			case('4'):
				if (!joc_definit)
					System.out.println("Primer has de definir la partida a la opció 2.");
				else
					Opcions.mostrarRanking();
				break;
			case('0'):
				acabat=true;
				break;
			}
		} while (!acabat);
		
	}
/**
 * Mostra les opcions del menú i vàlida l'entrada de l'usuari.
 * @return Retorna el valor que ha escollit l'usuari(1, 2, 3, 4 o 0).
 */
	private static char MostrarMenu() {
		char opcio;
		boolean correcte = false;
		do {
			System.out.println("\nMenú del pescamines\n");
			System.out.println("Escull una opció: ");
			System.out.println("1. Mostrar Ajuda.");
			System.out.println("2. Opcions.");
			System.out.println("3. Jugar Partida.");
			System.out.println("4. Veure Rankings.");
			System.out.println("0. Sortir.");
			System.out.print("\nEscull una opció: ");
			opcio = src.next().charAt(0);
			if (opcio=='1' || opcio=='2' || opcio=='3' || opcio == '4' || opcio == '0')
				correcte=true;
			else
				System.out.println("Has d'escollir una opció vàlida.");
		} while (!correcte);
		return opcio;
	}

}
